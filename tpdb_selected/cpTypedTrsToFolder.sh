#!/bin/bash

# This tools moves all files of subfolders with *typed.trs into the output folder.

OUT=typed

files=`find . -name "*.typed.trs" -and -not -path "./$OUT/*"`

mkdir -p $OUT

for f in $files;
do
    mkdir -p "$OUT/`dirname $f`"
    cp $f "$OUT/$f"
    # cp $f "$OUT/`basename $f`"
done

