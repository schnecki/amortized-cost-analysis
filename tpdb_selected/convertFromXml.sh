#!/bin/bash

# This tool needs the file xtc2tpdb.xsl in the current directory! It converts all
# xml files in any subfolder to corresponding TRSs.

files=`find . -name "*.xml"`

echo "Files: $files"

for f in $files;
do
    xsltproc xtc2tpdb.xsl $f > "${f%.xml}.trs"
done

