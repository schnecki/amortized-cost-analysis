{-# LANGUAGE DeriveDataTypeable #-}
-- TypeError.hs ---
--
-- Filename: TypeError.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Di Dez  3 00:42:09 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:53 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 114
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module defines the exception types for the program.
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:

module Exceptions.ExceptionHandler
    ( ProgException(..)
    ) where


import           Control.Exception (Exception)
import           Data.Typeable


-----------------------------------------------------------------------------
-- SYNONYMS
type ErrorTxt = String


-----------------------------------------------------------------------------
-- | Definition of the Exception Types used in this program
data ProgException = ShowTextOnly ErrorTxt
                       | WarningException ErrorTxt
                       | FatalException ErrorTxt
                       | ParseException ErrorTxt
                       | UnsolveableException ErrorTxt
                       | SemanticException ErrorTxt
                   deriving (Typeable)

-- | Make the data ProgException an instance of Exception
instance Exception ProgException


-- | Overwrites show for better error texts
instance Show ProgException where
    show (ShowTextOnly txt)      = txt
    show (SemanticException txt) = prefixSemantic ++ txt
    show (WarningException txt)  = prefixWarning ++  txt
    show (FatalException txt)    = prefixFatal ++ txt
    show (ParseException txt)    = prefixParse ++ txt


-----------------------------------------------------------------------------
-- HELPER FUNCTIONS


prefixWarning = "Warning: "
prefixFatal =  "FATAL ERROR: "
prefixParse =  "Parse Error: "
prefixSemantic = "Semantic Error: "
--
-- TypeError.hs ends here
