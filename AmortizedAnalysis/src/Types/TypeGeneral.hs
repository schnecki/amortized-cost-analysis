{-# LANGUAGE CPP #-}
-- TypeGeneral.hs ---
--
-- Filename: TypeGeneral.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: So Dez  8 17:41:38 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:50 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 302
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:

module Types.TypeGeneral where

import           Exceptions.ExceptionHandler
import           Misc.HelperFunctions
import           Types.TypeDatatype

import           Control.Exception           (throw)
#ifdef DEBUG
import           Debug.Trace                 (trace)
#endif

-----------------------------------------------------------------------------
-- | This data type stores the costs of a statement
data Cost = CostInt Int
          | CostVar String
          | CostEmpty
            deriving (Eq)

instance Show Cost where
    show (CostInt x) = show x
    show (CostVar x) = x
    show CostEmpty   = []

instance Num Cost where

    fromInteger = CostInt . fromInteger
    (+) a b = CostInt (nrOfCost a + nrOfCost b)
    (*) a b = CostInt (nrOfCost a * nrOfCost b)
    abs = fail "abs not defined"
    signum = fail "not defined"


nrOfCost :: Cost -> Int
nrOfCost (CostInt x) = x
nrOfCost (CostVar _) = throw $ FatalException "Nr of Variable in CostVar."
nrOfCost (CostEmpty) = 0


class ShowCost a where
    showCost :: a -> String

instance ShowCost Cost where
    showCost (CostInt x) = show x
    showCost (CostVar v) = v
    showCost CostEmpty   = throw $ FatalException
                           "COST WAS EMPTY. Programming error. Sry :( [Cost instance]"


-----------------------------------------------------------------------------
-- | This type represents internal variables, used for the constraints.
--   The Show instance is interesting here. It prints nothing, if getIVariable
--   is empty. It prints the variable name if getINumber equals (-1) and it
--   prints getINumber if there is a number set and the name is not empty.
--   getIVariable holds the variable name.
--   getINumber holds the number the variable is set to.
--
--   Eq is overwritten. It does not check the costs, but all other members for
--   equality.
data InternalVariable = InternalVariable
    { getIVVariable :: String
    , getIVDatatype :: Datatype
    , getIVCosts    :: [Cost]
    }

instance Show InternalVariable where
#ifdef DEBUG
    show (InternalVariable a d c) = a ++ " : " ++ show d ++ "(" ++ showListWithSep show c ", " ++ ")"
#endif
    show (InternalVariable "" d [])  = show d
    -- show (InternalVariable v NIL []) = v
    show (InternalVariable v _ []) = v     -- only used for displaying
                                           -- parsed rules, there is
                                           -- (show d == v)
    show (InternalVariable _ d n)    = show d ++ " (" ++ showListWithSep show n ", " ++ ")"


instance Eq InternalVariable where
    (==) (InternalVariable s1 NIL _) (InternalVariable s2 _  _)
        = s1 == s2
    (==) (InternalVariable s1 _ _) (InternalVariable s2 NIL _)
        = s1 == s2
    (==) (InternalVariable s1 d1 _) (InternalVariable s2 d2 _)
        = d1 == d2 && s1 == s2

-----------------------------------------------------------------------------

--
-- TypeGeneral.hs ends here

