-- TypeArgumentInfo.hs ---
--
-- Filename: TypeArgumentInfo.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 20:22:08 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:51 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 63
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module holds the information for the argument
-- information data object.
--
--

-- Change Log:
--
--
--

--
--

-- Code:

module Types.TypeArgumentOptions where

data ArgumentOptions = ArgumentOptions
    { getFilePath      :: FilePath
    , getVerbose       :: Bool
    , getMaxVariable   :: Int
    , getDebug         :: Bool
    , getMaxTreeHeight :: Int
    , getHelpText      :: Bool
    , getKeepFiles     :: Bool
    , getSMTSolver     :: Bool
    , getTempDir       :: String
    , getUseRelaxRule  :: Bool
    } deriving (Show, Eq)


--
-- TypeArgumentInfo.hs ends here
