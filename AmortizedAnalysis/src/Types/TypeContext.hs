-- TypeContext.hs ---
--
-- Filename: TypeContext.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Sa Dez 21 17:00:49 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:51 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 136
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:

module Types.TypeContext where


import           Misc.HelperFunctions
import           Types.TypeConstraint
import           Types.TypeDatatype
import           Types.TypeGeneral
import           Types.TypeTypedTRS


data Prove = Prove
    { getContexts       :: [Context]
    , getProvenContexts :: [Context]
    , getTRS            :: TypedTRS
    , getVarNr          :: Int
    }

instance Show Prove where
    show (Prove c ctx trs v) =
        "TRS:\n\n" ++ show trs ++
        "\n\nProve:\n\nContexts To Prove:\n * " ++
        showListWithSep show c "\n * " ++ "\n\nProved Contexts:\n * " ++
        showListWithSep show ctx "\n * " ++ "\n\nVariables: " ++ show v


data Context = Context
    { getPreConditions :: [InternalVariable]
    , getCosts         :: (Cost, Cost)
    , getConditions    :: Conditions
    , getPostCondition :: CtxStatement
    , getHistory       :: [(Int, String)]
    } deriving (Eq)

instance Show Context where
    show (Context pre c cond post history) =
         showListWithSep show pre ", "++ " |-" ++ show (fst c) ++ "/" ++ show (snd c) ++
                             "- " ++ show post ++ "\n\n\t"
                             ++ showListWithSep show history "\n\t" ++ "\n\n\t"
                             ++ show cond ++ "\n----------\n\n\t"


data CtxStatement = CtxFunction
    { getCtxFName     :: String
    , getCtxFDatatype :: Datatype
    , getCtxFFunction :: [CtxStatement]
    , getCtxFCost     :: [Cost]
    }
                  | CtxEmpty
                  | CtxAtom
    {
      getCtxAVariable :: InternalVariable
    } deriving (Eq)


instance Show CtxStatement where
    show (CtxFunction n NIL ch _) = n ++ "(" ++ showListWithSep show ch "," ++ ")"
    show (CtxFunction n d ch c) = n ++ "(" ++ showListWithSep show ch "," ++ ")" ++
                                  " : " ++ show d ++ "(" ++ showListWithSep show c ", " ++ ")"
    show (CtxAtom x)            = show x
    show CtxEmpty               = ""


data Conditions = Conditions
    {
      getVarUnificands   :: [(String, String)]
    , getTypeUnificands  :: [(Datatype, Datatype)]
    , getCostConstraints :: [Constraint]
    } deriving (Eq)

instance Show Conditions where
    show (Conditions v t c) = "Variable Constraints:\n\t" ++
                              showListWithSep show v ", " ++ "\n\n\t" ++
                              "Type Constraints:\n\t" ++
                              showListWithSep show t ", " ++ "\n\n\t" ++
                              "Cost Constraints:\n\t" ++
                              showListWithSep show c ", "

--
-- TypeContext.hs ends here
