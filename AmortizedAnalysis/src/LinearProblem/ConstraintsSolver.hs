{-# LANGUAGE CPP #-}
-- ConstraintsSolver.hs ---
--
-- Filename: ConstraintsSolver.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: So Dez  8 19:32:08 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:52 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 61
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:


module LinearProblem.ConstraintsSolver
    ( solveConstraints
    ) where


import           LinearProblem.LPSolve
import           SMTSolving.SMTSolver
import           Types.TypeArgumentOptions
import           Types.TypeConstraint

import qualified Data.Map.Strict           as Map

#ifdef DEBUG
import           Debug.Trace               (trace)
#endif


solveConstraints :: LPProblem -> ArgumentOptions -> IO (Map.Map String Int)
solveConstraints p opt =
    if getSMTSolver opt
    then solveInterpreationSMT opt sExpr
    else solveConstraintsLP p tempDir keepFiles -- use LP Solve

    where
      tempDir = getTempDir opt
      keepFiles = getKeepFiles opt
      sExpr = map constraintToSExpression (getPConstraints p)


--
-- ConstraintsSolver.hs ends here
