{-# LANGUAGE TemplateHaskell #-}
-- Type.hs ---
--
-- Filename: Type.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:32:10 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Thu May  5 21:40:25 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 36
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Constraint.Type where


import           Control.Lens

data Constraint m a = RightArrow -- a + b + c + 1 >= d + e
                      { _from :: [m]
                      , _dist :: a
                      , _to   :: [m]
                      }
                    | NEqArrow  -- a /= b OR [c] == [d]
                      { _nEqFrom :: m
                      , _nEqTo   :: m
                      , _nEqAlt  :: [(m, m)]
                      }
                    | RightEqArrow -- a = b + c + d
                      { _eqFrom :: m
                      , _eqTo   :: [m]
                      }
                    | MultArrow -- a1 = n * b1, a2 = n * b2
                      { _multArrFrom :: m
                      , _multArrN    :: String
                      , _multArrTo   :: m
                      } deriving (Eq,Read,Show)

makeLenses ''Constraint


-- instance (Show a, Show b) => Show (Constraint b a) where
--   show (RightArrow f d t) = show f ++ " --" ++ show d ++ "--> " ++ show t
--   show (NEqArrow f t alt) = show f ++ " /= " ++ show t ++ " OR " ++ show alt
--   show (RightEqArrow f t) = show f ++ " == " ++ show (map show t)
--   show (MultArrow f n t)  = show f ++ " == " ++ show n ++ " * " ++ show t


--
-- Type.hs ends here
