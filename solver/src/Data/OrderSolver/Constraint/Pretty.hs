-- Pretty.hs ---
--
-- Filename: Pretty.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 17:18:24 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fri May  6 11:05:02 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 25
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:


module Data.OrderSolver.Constraint.Pretty where

import           Data.OrderSolver.Assignment.Pretty
import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Util
import           Data.OrderSolver.Variable.Pretty

import           Data.List
import           Text.PrettyPrint

prettyConstraint :: [(String, Int, Assignment)] -> (a -> Doc) -> Constraint Int a -> Doc
prettyConstraint ns pA (RightArrow froms d tos) =
  hcat (punctuate (text "+") $ map int froms) <+>
  text "--" <> pA d <> text "-->" <+> hcat (punctuate (text "+") $
                                            map int tos)
prettyConstraint ns pA (NEqArrow f t alt) =
  int f <+> text "/=" <+> int t <+> text "OR" <+>
  parens (hcat (punctuate (text " ∧ ") $ map (\(a,b) -> int a <> text "==" <> int b) alt))
prettyConstraint ns pA (RightEqArrow f t) =
  int f <+> text "==" <+> hcat (punctuate (text "+") $
                                                map int  t)
prettyConstraint ns pA (MultArrow f n t) =
  int f <+> text "==" <+> text n <>
  text ":" <> getA n <+> text "*" <+> int t

  where getA n = case (find ((== n) . fst3) ns) of
                   Just (_,x,s) -> int x <> prettyAssignment s
                   Nothing      -> text "?"


--
-- Pretty.hs ends here
