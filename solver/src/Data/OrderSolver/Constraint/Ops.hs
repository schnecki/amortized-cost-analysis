-- Ops.hs ---
--
-- Filename: Ops.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:32:58 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Sun May  8 22:25:35 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 29
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Constraint.Ops where

import           Data.OrderSolver.Constraint.Type


getFroms :: Constraint t t1 -> [t]
getFroms (RightArrow f _ _) = f
getFroms (RightEqArrow f _) = [f]
getFroms (NEqArrow f _ _)   = [f]
getFroms (MultArrow f _ _)  = [f]


getTos :: Constraint t t1 -> [t]
getTos (RightArrow _ _ t) = t
getTos (RightEqArrow _ t) = t
getTos (NEqArrow _ t _)   = [t]
getTos (MultArrow _ _ t)  = [t]


isNEqArrow :: Constraint f t -> Bool
isNEqArrow (NEqArrow {}) = True
isNEqArrow _ = False

isConstraint :: Constraint f t -> Bool
isConstraint (RightArrow {}) = True
isConstraint _ = False

isRightArrow :: Constraint f t -> Bool
isRightArrow (RightArrow {}) = True
isRightArrow _ = False

isRightEqArrow :: Constraint f t -> Bool
isRightEqArrow (RightEqArrow {}) = True
isRightEqArrow _ = False

isMultArrow                :: Constraint f t -> Bool
isMultArrow (MultArrow {}) = True
isMultArrow _              = False


--
-- Ops.hs ends here
