{-# LANGUAGE FlexibleContexts #-}
-- Solve.hs ---
--
-- Filename: Sort.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 18:02:21 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May 18 14:43:37 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 149
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Solve
    ( solveOrderProblem
    ) where

import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Data.Type
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Problem.AnalyzeProblem
import           Data.OrderSolver.Problem.AssignNumbers
import           Data.OrderSolver.Problem.Pretty
import           Data.OrderSolver.Problem.SimplifyProblem
import           Data.OrderSolver.Problem.SortEqualValues
import           Data.OrderSolver.Problem.Type
import           Data.OrderSolver.Types


import           Control.Lens
import           Control.Monad.State
import           Data.Maybe

import           Debug.Trace
import           Text.PrettyPrint

solveOrderProblem :: (Enum a, Show a, OrderSolver a, Eq a, Num a, Ord a) =>
                    t
                  -> (a -> Doc)
                  -> Problem a
                  -> Maybe (Problem a)
solveOrderProblem _ pA problem =
  let sols =
        trace ("Problem:\n\n"++ show (prettyProblem pA problem)) $
        trace ("Show Problem:\n\n"++ show (show problem)) $
        execState (solveProblem pA) [problem]
  in if null sols
     then Nothing
     else Just (head sols)


solveProblem    :: (Show a, Ord a, Num a, Eq a, Enum a, OrderSolver a) =>
                  (a -> Doc) -> State [Problem a] ()
solveProblem pA = do
  simplifyProblems
  analyzeProblemsAndImplyConstraints
  sortEqualValues
  assignNumbers pA


--
-- Solve.hs ends here
