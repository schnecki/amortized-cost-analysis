{-# LANGUAGE FlexibleContexts #-}
-- Insert.hs ---
--
-- Filename: Insert.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:58:58 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May 11 12:49:18 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 667
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Insert
    ( insertNEqConstraint
    , insertGeqConstraint
    , insertEqConstraint
    , insertMultConstraint
    ) where

import Data.OrderSolver.Assignment.Type
import Data.OrderSolver.Constraint.Ops
import Data.OrderSolver.Constraint.Type
import Data.OrderSolver.EqualValues.Type
import Data.OrderSolver.Problem.Ops
import Data.OrderSolver.Problem.Type
import Data.OrderSolver.Util
import Data.OrderSolver.Variable.Ops
import Data.OrderSolver.Variable.Type

import Control.Lens                      hiding (Strict)
import Control.Monad.State
import Data.List

import Debug.Trace
import Text.PrettyPrint


-- ^ Insert a NEq constraint with alternatives. The first two variables cannot be
-- equal, or if they are then all tuples in the list of variables have to be equal.
insertNEqConstraint         :: (Monad m, Num t, Ord t, Show t) =>
                              Variable t
                            -> Variable t
                            -> [(Variable t, Variable t)]
                            -> Problem t
                            -> m (Problem t)
insertNEqConstraint v1 v2 alt p = flip execStateT p $ do
  v1Nr <- insertVarAndGetEvsNr v1
  v2Nr <- insertVarAndGetEvsNr v2
  alt' <- mapM (\(a,b) -> do
                a' <- insertVarAndGetEvsNr a
                b' <- insertVarAndGetEvsNr b
                return (a',b')) alt

  let nConstr = NEqArrow v1Nr v2Nr alt'
  insertConstraint nConstr


-- ^ Insert a Eq constraint, whereas the first variable is equal to the sum of the
-- list of variables.
insertEqConstraint :: (Eq a, Monad m, Ord a, Num a, Show a) =>
                      Variable a
                   -> [Variable a]
                   -> Problem a
                   -> m (Problem a)
insertEqConstraint v1 [v2] p = flip execStateT p $ do
  -- merge eqVals
  v1Nr <- insertVarAndGetEvsNr v1
  v2Nr <- insertVarAndGetEvsNr v2
  mergeEvs v1Nr v2Nr
insertEqConstraint v1 v2s p = flip execStateT p $ do
  v1Nrs <- insertVarAndGetEvsNr v1
  v2Nrs <- mapM insertVarAndGetEvsNr v2s
  let nConstr = RightEqArrow v1Nrs v2Nrs
  insertConstraint nConstr

-- ^ Insert a Geq Constraint whereas the first variable list is geq to the second
-- variable list.
insertGeqConstraint :: (Monad m, Num a, Ord a, Show a) =>
                      [Variable a]
                    -> [Variable a]
                    -> Problem a
                    -> m (Problem a)
insertGeqConstraint v1s v2s p = flip execStateT p $ do
  let v1sV = filter isVariable v1s
  let v2sV = filter isVariable v2s
  let v1sC = filter isConstant v1s
  let v2sC = filter isConstant v2s
  let nr = (sumConst v2sC) + (negate $ sumConst v1sC)
      sumConst = sum . map getConstant'

  v1Nrs <- mapM insertVarAndGetEvsNr v1sV
  v2Nrs <- mapM insertVarAndGetEvsNr v2sV

  when (null v1Nrs && null v2Nrs && nr /= 0)
    (fail "constraint itself is infeasible")
  if null v1Nrs && null v2Nrs    -- only numbers
    then put p                  -- revert to original, only numbers (e.g., 0 > 0)
    else do
    evsZeroNr <- getZeroEvsNr
    let nCconstr = RightArrow v1Nrs nr (if null v2Nrs then [evsZeroNr] else v2Nrs)
    insertConstraint nCconstr


-- ^ Insert a new Multiplier constraint. This means that the first variable equals
-- the second variables times the third variable, whereas the second one is
-- restricted to be an integer.
insertMultConstraint :: (Monad m, Ord t, Num t, Show t) =>
                       Variable t
                     -> Variable Int
                     -> Variable t
                     -> Problem t
                     -> m (Problem t)
insertMultConstraint v1 n v2 p
  | n == Constant 0 = flip execStateT p $ do
      evsZeroNr <- getZeroEvsNr
      v1Nr <- insertVarAndGetEvsNr v1
      mergeEvs evsZeroNr v1Nr
  | n == Constant{}  = fail "MultArr with n being a constant. This is not allowed."
  | otherwise = flip execStateT p $ do
      v1Nr <- insertVarAndGetEvsNr v1
      v2Nr <- insertMultiplierVarAndGetEvsNr v2
      n' <- insertNAndGetName n
      let nConstr = MultArrow v1Nr n' v2Nr
      insertConstraint nConstr

-- ^ This function inserts a new n from the MultArr and returns its name.
insertNAndGetName :: (Monad m, Show t, Eq t, Num t) =>
                    Variable Int
                  -> StateT (Problem t) m String
insertNAndGetName (Constant v) = do
  let name = internalNConstName v
  search <- gets (^.. (ns.traversed.filtered ((== name) . fst3)))
  when (null search)
    (modify (ns %~ ((name,v,Strict) :) ))
  return name
insertNAndGetName (Variable x) = do
  search <- gets (^.. (ns.traversed.filtered ((== x) . fst3)))
  when (null search)
    (modify (ns %~ ((x,0,Unassigned) :) ))
  return x


insertMultiplierVarAndGetEvsNr :: (Monad m, Eq t, Show t, Num t) =>
                                 Variable t
                               -> StateT (Problem t) m Int
insertMultiplierVarAndGetEvsNr var = do
  search <- gets (^.. multipliers.traversed.variables.traversed.filtered (== var))
  when (null search)
    (insertNewMultiplier var)
  getMultiplierNr var


-- ^ This function inserts variables into a problem and returns the EqualValues Nr
-- in which it is. In case the variable already exists, it will not be added again,
-- but just the evsNr will be returned. Variables cannot start with
-- `internalVarPrefix` defined in the module Data.OrderSolver.Util!
insertVarAndGetEvsNr :: (Monad m, Eq t, Show t, Num t) =>
                       Variable t
                     -> StateT (Problem t) m Int
insertVarAndGetEvsNr var = case var of
    Constant _ -> insertVarAndGetNr'
    Variable n -> do
      when (take (length internalVarPrefix) n == internalVarPrefix)
        (error $ "Variables cannot start with: " ++ show internalVarPrefix)
      insertVarAndGetNr'

  where insertVarAndGetNr' = do
          search <- gets (^.. eqVals.traversed.variables.traversed.filtered (== var))

          -- if not found then insert new var
          when (null search)
            (insertNewEvs var)

          -- get evs
          getEvsNr var


-- ^ This function creates and inserts a new evs into the Problem.
insertNewEvs     :: (Monad m, Num t) => Variable t -> StateT (Problem t) m ()
insertNewEvs var = do
  nr <- getNewEqValsNr
  let nEvs = EqualValues nr [var] 0 [] Unassigned
  modify (eqVals %~ (nEvs:))       -- insert new Evs


-- ^ This function creates and inserts a new multiplier into the problem.
insertNewMultiplier     :: (Monad m, Num t) => Variable t -> StateT (Problem t) m ()
insertNewMultiplier var = do
  nr <- getNewEqValsNr
  let nEvs = EqualValues nr [var] 0 [] Unassigned
  modify (multipliers %~ (nEvs :))


-- ^ This function retrieves the current evsNr for new insertions and increments
-- it, before returning the number.
getNewEqValsNr :: (Monad m, Num t) => StateT (Problem t) m Int
getNewEqValsNr = do
  nr <- gets (view eqValsCounter)
  eqValsCounter += 1
  return nr


--
-- Insert.hs ends here
