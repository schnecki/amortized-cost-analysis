-- Types.hs ---
--
-- Filename: Types.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 17:38:08 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May  4 17:38:54 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 6
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Types where


class OrderSolver a where
  times :: Int -> a -> a

instance OrderSolver Int where
  times x y = x * y


--
-- Types.hs ends here
