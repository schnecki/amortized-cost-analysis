-- Ops.hs ---
--
-- Filename: Ops.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:38:35 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Tue May 24 15:18:54 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 37
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:


module Data.OrderSolver.Data.Ops where

import           Data.OrderSolver.Data.Type
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Problem.Type
import           Data.OrderSolver.Variable.Ops

import           Data.List
import           Data.OrderSolver.Util

getData :: (Num a, Show a) => Problem a -> ([Data Int], [Data a])
getData (Problem sorted unsorted _ _ ctrs ns _ rs _) =

  (map (\(n, nr, _) -> Data n nr) (ns ++ rs')
  , concatMap (\(EqualValues _ vs v _ _) ->
                map (flip Data v . getVariableName) vs
                -- map (flip Data v . show . getConstant') (filter isConstant vs)
              ) $ ctrs ++ sorted ++ unsorted )
  where rs' = foldl (\acc (b,a) ->
                      case find ((== a) . fst3) ns of
                        Nothing -> acc
                        Just (_,f,g) -> (b,f,g) : acc) [] rs


--
-- Ops.hs ends here
