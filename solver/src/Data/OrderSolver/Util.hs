-- Util.hs ---
--
-- Filename: Util.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:42:02 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mon May  9 19:40:18 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 165
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:


module Data.OrderSolver.Util
    ( fst3
    , snd3
    , thd3
    , fst4
    , snd4
    , thd4
    , fth4
    , logAnd
    , logOr
    , internalVarPrefix
    , internalNConstName
    , getReplacementListFromTuples
    , breathFirstSearchWoStart
    , breathFirstSearchWithAccFun
    , breathFirstSearchPathWoFirst
    , bfs
    ) where

import           Control.Arrow
import           Data.Function (on)
import           Data.List
import           Data.Maybe

import           Debug.Trace

internalVarPrefix :: String
internalVarPrefix = "ipvar_solver_"

internalNConstName :: Show a => a -> String
internalNConstName v = internalVarPrefix ++ "const_" ++ show v


fst3 :: (a,b,c) -> a
fst3 (a,_,_) = a

snd3 :: (a,b,c) -> b
snd3 (_,b,_) = b

thd3         :: (a,b,c) -> c
thd3 (_,_,c) = c


fst4 :: (t, t1, t2, t3) -> t
fst4 (a,_,_,_) = a

snd4 :: (t, t1, t2, t3) -> t1
snd4 (_,b,_,_) = b

thd4 :: (t, t1, t2, t3) -> t2
thd4 (_,_,c,_) = c

fth4 :: (t, t1, t2, t3) -> t3
fth4 (_,_,_,d) = d


logAnd :: [a -> Bool] -> a -> Bool
logAnd fs x = all (\f -> f x) fs

logOr :: [a -> Bool] -> a -> Bool
logOr fs x = any (\f -> f x) fs

getReplacementListFromTuples :: Ord t => [(t, t)] -> [(t, t)]
getReplacementListFromTuples xs = replaceLaterOccurances [] repls
  where alts       = map sortTupleElems xs
        altGroups  = groupBy ((==) `on` fst) $ sortBy (compare `on` fst) alts
        altGroups' = map makeTransitiv altGroups
        repls      = concatMap replList altGroups'


makeTransitiv    :: [(t, t)] -> [(t, t)]
makeTransitiv [] = []
makeTransitiv [x] = [x]
makeTransitiv ((a,b):xs) =
  (a,b) : makeTransitiv' b xs
  where makeTransitiv' _ []          = []
        makeTransitiv' nr ((a,b):ys) = (nr,b):makeTransitiv' b ys

sortTupleElems :: (Ord t) => (t,t) -> (t,t)
sortTupleElems (a,b)
  | a <= b      = (a,b)
  | otherwise  = (b,a)


replList    :: [(t,t)] -> [(t,t)]
replList xs = foldl fun [] xs
  where e             = snd (last xs)
        fun acc (a,_) =  (a,e):acc

replaceLaterOccurances                :: Eq t => [(t,t)] -> [(t,t)] -> [(t,t)]
replaceLaterOccurances acc []         = acc
replaceLaterOccurances acc ((a,b):xs) =
                              replaceLaterOccurances (acc++[(a,b)])
                              (map (\(x,y) -> if x == a then (b,y) else (x,y)) xs)


                              -- GRAPH SEARCH ALGORITHMS

breathFirstSearchPath :: (Eq a) => [(a, [a])] -> a -> ([a] -> a -> Bool) -> [[a]]
breathFirstSearchPath jumps start pEnd = map reverse $ breathFirstSearchPath' [] [[start]]
    where breathFirstSearchPath' acc [] = acc
          breathFirstSearchPath' acc (x:xs) =
              case x of
                [] -> breathFirstSearchPath' acc xs
                (y:ys)
                    | pEnd x y -> breathFirstSearchPath' (x:acc) xs
                    | otherwise ->
                        case find ((==y) . fst) jumps of
                          Nothing -> breathFirstSearchPath' acc xs
                          Just (_,ls) ->
                            breathFirstSearchPath' acc (map (:x)
                                                        (filter (`notElem` ys) ls) ++ xs)

breathFirstSearchPathWoFirst :: (Eq a) =>
                               [(a, [a])]
                             -> ([a] -> a -> Bool)
                             -> a
                             -> [[a]]
breathFirstSearchPathWoFirst jumps pEnd start =
  case find ((==start) . fst) jumps of
    Nothing -> []
    Just (_,ls) -> map reverse $ breathFirstSearchPath' [] [ls]
    where breathFirstSearchPath' acc [] = acc
          breathFirstSearchPath' acc (x:xs) =
              case x of
                [] -> breathFirstSearchPath' acc xs
                (y:ys)
                    | pEnd x y -> breathFirstSearchPath' (x:acc) xs
                    | otherwise ->
                        case find ((==y) . fst) jumps of
                          Nothing -> breathFirstSearchPath' acc xs
                          Just (_,ls) ->
                            breathFirstSearchPath' acc (map (:x)
                                                        (filter (`notElem` ys) ls) ++ xs)


breathFirstSearchPathWithFirstLevel :: (Eq a) => [(a, [a])] -> a -> ([a] -> a -> Bool) -> [([a],[a])]
breathFirstSearchPathWithFirstLevel jumps start pEnd =
    map (first reverse) $
        breathFirstSearchPath' [] [([start],[])]
    where breathFirstSearchPath' acc [] = acc
          breathFirstSearchPath' acc ((x,rs):xs) =
              case x of
                [] -> breathFirstSearchPath' acc xs
                (y:ys)
                    | pEnd x y -> breathFirstSearchPath' ((x,rs):acc) xs
                    | otherwise ->
                        case find ((==y) . fst) jumps of
                          Nothing -> breathFirstSearchPath' acc xs
                          Just (_,ls) ->
                              breathFirstSearchPath' acc (map (\l -> (l:x, delete l ls ++ rs))
                                                          (filter (`notElem` ys) ls) ++ xs)


breathFirstSearch :: (Eq a) => [(a, [a])] -> a -> (a, [a])
breathFirstSearch jumps start = (start, reverse $ breathFirstSearch' [] [start])
    where breathFirstSearch' acc [] = acc
          breathFirstSearch' acc elems@(x:xs) =
              case find ((== x) . fst) jumps of
                Nothing -> breathFirstSearch' (x:acc) xs
                Just (_,ls) -> breathFirstSearch' (x:acc)
                              (xs ++ filter (`notElem` (acc ++ elems)) ls)

breathFirstSearchWoStart :: (Eq a) => [(a, [a])] -> a -> (a, [a])
breathFirstSearchWoStart jumps start =
  case find ((== start) . fst) jumps of
    Nothing -> (start, [])
    Just (_,ls) -> (start, reverse $ breathFirstSearch' [] ls)
    where breathFirstSearch' acc [] = acc
          breathFirstSearch' acc elems@(x:xs) =
              case find ((== x) . fst) jumps of
                Nothing     -> breathFirstSearch' (x:acc) xs
                Just (_,ls) -> breathFirstSearch' (x:acc)
                              (xs ++ filter (`notElem` (acc ++ elems)) ls)

breathFirstSearchWithAccFun :: (Eq a) =>
                              ((a,b) -> [(a,b)] -> [(a,b)])
                              -> [(a, [(a,b)])]
                              -> a
                              -> (a, [(a,b)])
breathFirstSearchWithAccFun accFun jumps start =
  case find ((== start) . fst) jumps of
    Nothing -> (start, [])
    Just (_,ls) -> (start, reverse $ breathFirstSearch' [] ls)
    where breathFirstSearch' acc [] = acc
          breathFirstSearch' acc elems@(x:xs) =
              case find ((== fst x) . fst) jumps of
                Nothing     -> breathFirstSearch' (x:acc) xs
                Just (_,ls) -> breathFirstSearch' (x:acc') xs'

                  where (acc',xs') = foldl fun (acc,xs) ls
                        fun (accAcc,accXs) e
                          | isJust $ find ((== fst e) . fst) (accXs ++ accAcc) =
                              (accFun e accAcc, accFun e xs)
                          | otherwise = (accAcc, e:xs)

replaceCost         :: (Eq a, Ord b) => (a, b) -> [(a, b)] -> [(a, b)]
replaceCost x xs    = map (replaceCost' x) xs
  where replaceCost' (nr, cst) (nr2, cst2)
          | nr == nr2  = (nr, max cst cst2)
          | otherwise = (nr2, cst2)


bfs :: (Eq b, Eq a, Show a, Show b) =>
      ((a,b) -> [(a,b)] -> [(a,b)])
    -> [(a, [(a,b)])] -- (from, [to,acc])
    -> a              -- start
    -> (a, [(a,b)])
bfs fun jumps start =
  case find ((==start) . fst) jumps of
    Nothing       -> (start, [])
    Just (_,jmps) -> (start, foldl (bfs' jmps) jmps jmps)
  where bfs' visited acc node@(nr,b) = case find ((== nr) . fst) jumps of
          Nothing     -> acc
          Just (_,jmps) ->
                      foldl (bfs' (node:visited)) (acc++fun node' jmps)
                      (filter (`notElem` visited) jmps)
          where node' = fromJust $ find ((== fst node) . fst) acc


--
-- Util.hs ends here
