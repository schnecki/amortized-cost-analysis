{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes       #-}
-- Ops.hs ---
--
-- Filename: Ops.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:36:54 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May 18 12:18:22 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 123
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Problem.Ops where

import           Data.OrderSolver.Assignment.Ops
import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Ops
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.EqualValues.Ops
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Problem.Type
import           Data.OrderSolver.Util
import           Data.OrderSolver.Variable.Ops
import           Data.OrderSolver.Variable.Type

import           Control.Lens                      hiding (Strict)
import           Control.Monad
import           Control.Monad.State
import           Data.List
import           Data.Maybe

import           Debug.Trace
import           Text.PrettyPrint.ANSI.Leijen


-- ^ This function retrieves the evsNr that includes the Constant 0.
getZeroEvsNr :: (Monad m, Eq t, Show t, Num t) =>
               StateT (Problem t) m Int
getZeroEvsNr = getEvsNr (Constant 0)

-- ^ Retrieves the EqualValues which has Constant 0 as variable.
getZeroEvs :: (Monad m, Eq t, Show t, Num t) =>
             StateT (Problem t) m (EqualValues t)
getZeroEvs = do
  nr <- getZeroEvsNr
  getEvs nr


-- ^ get EqualValues by evsNr (unsafe)
getEvs :: (Monad m, Eq t, Show t, Num t) =>
        Int
      -> StateT (Problem t) m (EqualValues t)
getEvs nr =
  gets (^?! eqVals.traversed.filtered (\x -> view evsNr x == nr))

-- ^ Get evsNr by Variable (unsafe)
getEvsNr :: (Monad m, Eq t, Show t, Num t) =>
           Variable t
         -> StateT (Problem t) m Int
getEvsNr var =
  gets (^?! eqVals.traversed.filtered
        (\x -> var `elem` (view variables x)).evsNr)

getMultiplierNr :: (Monad m, Eq t, Show t, Num t) =>
                  Variable t
                -> StateT (Problem t) m Int
getMultiplierNr var =
  gets (^?! multipliers.traversed.filtered
        (\x -> var `elem` (view variables x)).evsNr)

getMultiplier :: (Monad m, Eq t, Show t, Num t) =>
                Int
              -> StateT (Problem t) m (EqualValues t)
getMultiplier nr =
  gets (^?! multipliers.traversed.filtered (\x -> nr == view evsNr x))


-- ^ This function merges the two EqualValues given by their evsNr together.
mergeEvs :: (Monad m, Show t, Num t, Eq t) =>
           Int
         -> Int
         -> StateT (Problem t) m ()
mergeEvs v1Nr v2Nr
  | v1Nr == v2Nr                            = return ()      -- nothing to do
  | otherwise                              = do

      v1Evs <- getEvs v1Nr
      v2Evs <- getEvs v2Nr

      when (strictlySetToDifferentValues v1Evs v2Evs)
        (fail "infeasible")

      if notEqualValues v1Evs v2Evs
        then                    -- alternatives must be enforced
             let arrow = (v1Evs ^?! constraints.traversed.filtered isNEqArrow.
                          filtered (\x -> v2Nr == x ^?! nEqTo ||
                                         v2Nr == x ^?! nEqFrom))
                 repls = getReplacementListFromTuples (view nEqAlt arrow)
             in mapM_ (uncurry mergeEvs) repls
        else do                 -- merge the EqualValues

        deleteEvs v1Nr
        deleteEvs v2Nr

        let evsNew = EqualValues v1Nr (v1Evs ^. variables ++ v2Evs ^. variables)
              0 (v1Evs ^. constraints ++ v2Evs ^. constraints)
              (mergeAssignments (view assignment v1Evs) (view assignment v2Evs))

        addEvs evsNew
        renameEvsNr v2Nr v1Nr

-- ^ Deletes a EqualValues according to the evsNr.
deleteEvs :: (Monad m, Show t, Eq t, Num t) =>
            Int
          -> StateT (Problem t) m ()
deleteEvs nr = modify (eqVals %~ filter ((/= nr) . view evsNr))

-- ^ Adds a EqualValues.
addEvs :: (Monad m, Show t, Eq t, Num t) =>
         EqualValues t
       -> StateT (Problem t) m ()
addEvs evs = modify (eqVals %~ (evs:))

-- ^. Renames all occurrences of a evsNr from the first parameter to the second.
renameEvsNr :: (Monad m, Show t, Eq t, Num t) =>
              Int
            -> Int
            -> StateT (Problem t) m ()
renameEvsNr from to = do
  let renameFun x = if x == from then to else x

  -- evsNr
  modify (eqVals.traversed.evsNr %~ renameFun )

  -- constraints
  let renameConstraint (RightArrow f d t) =
        RightArrow (map renameFun f) d (map renameFun t)
      renameConstraint (NEqArrow f t alt) =
        NEqArrow (renameFun f) (renameFun t) (map (over both renameFun) alt)
      renameConstraint (RightEqArrow f t) =
        RightEqArrow (renameFun f) (map renameFun t)
      renameConstraint (MultArrow f n t) =
        MultArrow (renameFun f) n (renameFun t)

  modify (eqVals.traversed.constraints.traversed %~ renameConstraint )

    -- register rename
  modify (renamesEvs %~ ((from,to):))

-- ^ This function inserts the given constraint, but first preforms checks to find
-- out weather it makes the problem infeasible or not.
insertConstraintLens :: (Monad m, Show t, Eq t, Num t) =>
                       Traversal' (Problem t) [EqualValues t]
                     -> Constraint Int t
                     -> StateT (Problem t) m ()
insertConstraintLens lens constr@(RightArrow fs d ts) =
  modify (lens.traversed.filtered (\x -> view evsNr x `elem` fs).
          constraints %~ (constr:))

insertConstraintLens lens constr@(NEqArrow f t alts) =
  if f == t
  then -- alternatives must hold
    mapM_ (uncurry mergeEvs) (getReplacementListFromTuples alts)
  else do
    modify (lens.traversed.
            filtered (\x -> view evsNr x == f).
            constraints %~ (constr:))
    let constr' = NEqArrow t f alts
    modify (lens.traversed.
            filtered (\x -> view evsNr x == t).
            constraints %~ (constr':))

insertConstraintLens lens constr@(RightEqArrow f t) =
  modify (lens.traversed.filtered (\x -> view evsNr x == f).
          constraints %~ (constr:))

insertConstraintLens lens constr@(MultArrow f n t) = do
  modify (lens.traversed.filtered (\x -> view evsNr x == f ).
          constraints %~ (constr:))

-- ^ Inserts a constraint in the eqVals EqualValues list.
insertConstraint :: (Monad m, Show t, Eq t, Num t) =>
                   Constraint Int t
                   -> StateT (Problem t) m ()
insertConstraint = insertConstraintLens eqVals


-- ^ Inserts a constraint in the sortedEqVals EqualValues list.
insertConstraintSorted :: (Monad m, Show t, Eq t, Num t) =>
                         Constraint Int t
                       -> StateT (Problem t) m ()
insertConstraintSorted = insertConstraintLens sortedEqVals

-- ^ Inserts a constraint in the sortedEqVals EqualValues list.
insertConstraintUnsorted :: (Monad m, Show t, Eq t, Num t) =>
                         Constraint Int t
                       -> StateT (Problem t) m ()
insertConstraintUnsorted = insertConstraintLens unsortedEqVals


  -- modify (multipliers.traversed.filtered (\x -> view evsNr x == t ).
  --         constraints %~ (constr:))

-- getAllVariables :: Problem a -> [Variable a]
-- getAllVariables (Problem eqVs ctrs ns _ _ _) =
--   map (Variable . fst3) ns ++ concatMap (view variables) (ctrs ++ eqVs)

-- toVariables                          :: Problem a -> [Variable a]
-- toVariables (Problem eqVs _ _ _ _) =
--   filter isVariable (concatMap (view variables) eqVs)


-- getEqVals :: Eq a => Variable a -> Problem a -> Maybe (EqualValues a)
-- getEqVals l ord = find ((elem l) . (view variables)) (view eqVals ord)

-- -- getEqVals' :: Eq a => Variable a -> Problem a -> EqualValues a
-- getEqVals' v ord =
--   if isNothing (getEqVals v ord)
--   then trace ("isJust getEqVals': " ++ show (isJust $ getEqVals v ord)) $
--        trace ("v: " ++ show v) $
--        trace ("ord: " ++ show (pretty ord))
--        trace ("UNDEFINED IN FUNCTION getEqVals'")
--        undefined
--   else fromJust $ getEqVals v ord

-- getEqValsEvsNr :: Int -> Problem a -> Maybe (EqualValues a)
-- getEqValsEvsNr no (Problem evs _ _ _ _) = find ((==no) . (view evsNr)) evs

-- getEqValsEvsNr' :: Int -> Problem a -> EqualValues a
-- getEqValsEvsNr' no ord =
--   if isNothing (getEqValsEvsNr no ord)
--   then trace ("isJust getEqVals': " ++ show (isJust (getEqValsEvsNr no ord))) $
--        trace ("no: " ++ show no) $
--        -- trace ("ord: " ++ show (pretty $ eqVals ord))
--        trace ("UNDEFINED IN FUNCTION getEqVals'")
--        undefined
--   else fromJust (getEqValsEvsNr no ord)

-- getEqValsValue :: Int -> Problem a -> Maybe a
-- getEqValsValue no ord = liftM (view value) (getEqValsEvsNr no ord)

-- getEqValsValue' :: Int -> Problem a -> a
-- getEqValsValue' no ord =
--   -- trace ("no: " ++ show no)
--   -- trace ("ord: " ++ show (map evsNr $ eqVals ord)) $
--   -- trace ("isJust getEqValsValue': " ++ show (isJust $ getEqValsValue no ord))
--   fromJust (getEqValsValue no ord)


-- getNsValue :: String -> Problem a -> Maybe Int
-- getNsValue n (Problem _ ns _ _ _) = liftM snd3 $ find ((== n) . fst3) ns

-- getNsValue' :: String -> Problem a -> Int
-- getNsValue' n ord =
--             -- trace ("isJust getNsValue': " ++ show (isJust $ getNsValue n ord)) $
--             fromJust $ getNsValue n ord


--
-- Ops.hs ends here
