-- AssignNumbers.hs ---
--
-- Filename: AssignNumbers.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mon May  9 16:49:54 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May 18 09:43:44 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 1389
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Problem.AssignNumbers
  ( assignNumbers
  ) where

import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Ops
import           Data.OrderSolver.Constraint.Pretty
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.Data.Type
import           Data.OrderSolver.EqualValues.Ops
import           Data.OrderSolver.EqualValues.Pretty
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Insert
import           Data.OrderSolver.Problem.Ops
import           Data.OrderSolver.Problem.Pretty
import           Data.OrderSolver.Problem.Type
import           Data.OrderSolver.Types
import           Data.OrderSolver.Util

import           Control.Arrow
import           Control.Lens                        hiding (Strict, from, to)
import           Control.Monad.State
import           Data.Function                       (on)
import           Data.List
import           Data.Maybe

import           Debug.Trace
import           Text.PrettyPrint


numbersToTake :: Int
numbersToTake = 10

assignNumbers :: (Monad m, Show a, Enum a, OrderSolver a, Num a, Ord a) =>
                (a -> Doc) -> StateT [Problem a] m ()
assignNumbers pA = do

  -- run for every Problem in the list
  modify (concat . concatMap (evalStateT (assignNumbers' pA)))


assignNumbers' :: (Monad m, Show a, Enum a, OrderSolver a, Num a, Ord a) =>
                (a -> Doc) -> StateT (Problem a) m [Problem a]
assignNumbers' pA = do
  unsorted <- gets (^. unsortedEqVals)
  unless (null unsorted)
             (trace ("system was not fully sorted!")
              fail "system was not fully sorted")

  evsNrs <- gets (^.. sortedEqVals.traversed.evsNr)

  p <- get

  let start = if null evsNrs
              then Nothing
              else Just (last evsNrs)

  trace ("\n\nAssignNumbers Problem: " ++ show (prettyProblem pA p))
        assignNumberLoop pA start

-- ^ This function loops while not all numbers are assigned.
assignNumberLoop :: (Monad m, Show a, Enum a, OrderSolver a, Num a, Ord a) =>
                   (a -> Doc)
                 -> Maybe Int
                 -> StateT (Problem a) m [Problem a]
assignNumberLoop pA Nothing = do
  p <- get
  return [p]
assignNumberLoop pA (Just evsNr) = do
  xs <- assignNumberTo pA evsNr
  return $ concat (concatMap (\(nr,p) -> evalStateT (assignNumberLoop pA nr) p) xs)

-- ^ This function splits the sortedEqVals in the ones before the given evsNr and
-- the one afterwards.
getBeforeAfter :: (Monad m) =>
                 Int
               -> StateT (Problem a) m ([Int], [Int])
getBeforeAfter evsToAssign = do
  -- get all evsNrs
  evsNrs <- gets (^..sortedEqVals.traversed.evsNr)

  -- split by evsToAssign
  let splitEvs (l,r,b) x
        | x == evsToAssign = (l,r,True)
        | b               = (l,r++[x],b)
        | otherwise       = (l++[x],r,b)
  let (before, after,_) = foldl splitEvs ([],[],False) evsNrs

  return (before,after)

-- ^ This function assign a number to the given evsToAssign. It considers all
-- arrays for this. The return value is a list of problems and their next evs
-- (specified by the evsNr to be assigned to).
assignNumberTo       :: (Monad m, Show a, Enum a, OrderSolver a, Num a, Ord a) =>
                       (a -> Doc)
                     -> Int
                     -> StateT (Problem a) m [(Maybe Int,Problem a)]
assignNumberTo pA evsToAssign = do

  -- find out before and after
  (before, after) <- getBeforeAfter evsToAssign
  let lastBef = if null before then Nothing else Just (last before)
  let lastAfter = if null after then Just evsToAssign else Just (last after)


  let importantArrs (RightArrow f d t) = all (`elem` after) (delete evsToAssign f)
      importantArrs (RightEqArrow f t) = True           -- all t must be in after
      importantArrs (NEqArrow f t alt) = t `elem` after -- only if topmost one and
                                                        -- alternatives do not hold
      importantArrs (MultArrow f n t) = True            -- always

  -- get evs
  -- evs <- gets (^?! sortedEqVals.traversed.filtered ((evsToAssign ==) . view evsNr))

  -- get important arrows from evs
  arrs <- gets (^.. sortedEqVals.traversed.filtered ((evsToAssign ==) . view evsNr).
               constraints.traversed.filtered importantArrs)

  -- get possible numbers from arrows
  p <- get
  let getNumbersCall constr = evalState (getNumbers evsToAssign constr) p
  let numsArrs = map getNumbersCall arrs

  -- get possible numbers from temporarily removed constraints
  (numsRemConstrArrs, solutionsRemConstr) <-
    getNumbersFromRemovedConstr pA evsToAssign

  let nums =
        take numbersToTake $
        if null arrs
        then [0..]
        else foldl intersectSorted [0..] (numsArrs ++
                                          if null numsRemConstrArrs
                                          then []
                                          else [numsRemConstrArrs])

  if null nums
    then
    trace ("numsArrs: " ++ show numsArrs)
    trace ("numsRemConstrArrs: " ++ show numsRemConstrArrs) $

    trace ("fail: No more options to assign to this evs!")
    -- fail "No more options to assign to this evs"
    trace ("null solutionsRemConstr: " ++ show (null solutionsRemConstr)) $
    return $ map (\sol -> (lastAfter, sol)) solutionsRemConstr
    else do
    -- get best number and assign
    let nr = head nums

    let multArrs = filter isMultArrow arrs
    let multArrVals = map head $ map getNumbersCall multArrs
    let multArrEqual = all (== (head multArrVals)) (tail multArrVals)
    let multArrValEqNr = nr == (head multArrVals)

    -- set new value
    trace ("evs " ++ show evsToAssign ++ " --> " ++ show nr ++ "   " ++ show nums)
          setVal evsToAssign nr

    if null multArrs || (multArrValEqNr && multArrEqual)
      then do
          p <- get               -- get current problem
          -- trace ("lastBef: " ++ show lastBef)
          return $ (lastBef,p) : map (\sol -> (lastAfter, sol)) solutionsRemConstr
      else do setMultArrNsAndToVal evsToAssign nr
              p <- get           -- get current problem
              multNs <- gets (^. ns)
              mults <- gets (^. multipliers)
              trace ("ns/multiplier change!")
                trace ("ns:\n" ++ show multNs) $
                trace ("multipliers:\n" ++
                       unlines (map (show . prettyEqualValues multNs pA) mults) ++"\n")
                    return $ (lastAfter,p) : -- add possible solutions
                           map (\sol -> (lastAfter, sol)) solutionsRemConstr

-- ^ Sets the value of a EqualValues (given by it's evsNr) in the sortedEqVals to
-- the given value. In case setting the value is not allowed, as the EqualValues is
-- labeled Strict then an error gets thrown.
setVal :: (Monad m, Num a, Ord a, Enum a, Show a, OrderSolver a) =>
         Int
       -> a
       -> StateT (Problem a) m ()
setVal evs val = do

  -- check if setting val is allowed
  ass <- gets (^?!sortedEqVals.traversed.filtered ((== evs) . view evsNr).assignment)
  curVal <- gets (^?! sortedEqVals.traversed.filtered ((== evs) . view evsNr).value)
  when (ass == Strict && val /= curVal)
    (error "cannot set value of strict evs")

  -- set val and assignment
  sortedEqVals.traversed.filtered ((== evs) . view evsNr).value .= val
  sortedEqVals.traversed.filtered ((== evs) . view evsNr).assignment .= Loosely

-- ^ This function sets the ns and multipliers occurring in the given EqualValues
-- (given by its evsNr 'evs') to the specified value 'val'. In case this is not
-- possible an error is thrown (so ensure that at least one combination is
-- possible).
setMultArrNsAndToVal :: (Monad m, Show a, Num a, Ord a, Enum a, OrderSolver a) =>
         Int
       -> a
       -> StateT (Problem a) m ()
setMultArrNsAndToVal evs val = do
  ns <- gets (^.. sortedEqVals.traversed.filtered ((==evs) . view evsNr).
             constraints.traversed.filtered isMultArrow.multArrN)
  multipliers <- gets (^.. sortedEqVals.traversed.filtered ((==evs) . view evsNr).
                      constraints.traversed.filtered isMultArrow.multArrTo)

  let combs = multArrCombs val
  let nsMult = zip ns multipliers
  p <- get
  let nsMultCurrent =
        map (\(a,b) -> (evalState (getNsTupleUnsafe a) p,
                       evalState (getMultiplier b) p)) nsMult

  -- sort by ns and mult
  let nsMultCurrentGroupNs =
        groupBy ((==) `on` (fst3.fst)) $
        sortBy (compare `on` (fst3.fst)) nsMultCurrent
  let nsMultCurrentGroupMult =
        groupBy ((==) `on` (view evsNr.snd)) $
        sortBy (compare `on` (view evsNr.snd)) nsMultCurrent

  -- get nrs from combinations for every ns which work with all MultArrs
  let nsCombs = map (\xs ->
                      ( fst3 $ fst (head xs)
                      , map fst $
                        foldl (\cmb ((_,v,_),mult) ->
                                filter (\(a,b) -> a>=v && b>=(view value mult))cmb)
                        combs xs)) nsMultCurrentGroupNs

  -- get nrs from combinations for every EqualValues which work with all MultArrs
  let multCombs = map (\xs ->
                      ( view evsNr $ snd (head xs)
                      , map snd $
                        foldl (\cmb ((_,v,_),mult) ->
                                filter (\(a,b) -> a>=v && b>=(view value mult))cmb)
                        combs xs)) nsMultCurrentGroupMult

  when (any (\(_,xs) -> null xs) nsCombs || any (\(_,xs) -> null xs) multCombs)
    (error "cannot set ns and multipliers") -- should not happen!!

  mapM_ (setNs . second head) nsCombs
  mapM_ (setMultiplier . second head) multCombs

  -- trace ("combs: " ++ show combs)
  --   trace ("nsMultCurrent: " ++ show nsMultCurrent)
  --   trace ("nsMultCurrentGroupNs: " ++ show nsMultCurrentGroupNs)
  --   trace ("nsCombs: " ++ show nsCombs)
  --   trace ("nsMultCurrentGroupMult: " ++ show nsMultCurrentGroupMult)
  --   trace ("multCombs: " ++ show multCombs)
  --   undefined

  sortedEqVals.traversed.filtered ((== evs) . view evsNr).value .= val

-- ^ This function sets the given ns to the given value.
setNs :: (Monad m) => (String,Int) -> StateT (Problem a) m ()
setNs (name,val) = do
  (_,_,ass) <- gets (^?! ns.traversed.filtered ((==name) . fst3))
  when (ass == Strict)
    (error "Cannot assign to as strict ns")

  ns.traversed.filtered ((==name) . fst3) .= (name,val,Loosely)

-- ^ This function sets a multiplier to the given value.
setMultiplier :: (Monad m) => (Int,a) -> StateT (Problem a) m ()
setMultiplier (nr,val) = do
  ass <- gets (^?! multipliers.traversed.filtered ((==nr) . view evsNr).assignment)
  when (ass == Strict)
    (error "Cannot assign to as strict ns")

  multipliers.traversed.filtered ((==nr) . view evsNr).value .= val
  multipliers.traversed.filtered ((==nr) . view evsNr).assignment .= Loosely


-- ^ Get a list of possible numbers the given evs can be assigned to, according to
-- the given constraint. For MultArrow the ns and t values may need adaption.
getNumbers :: (Monad m, Num a, Ord a, Enum a, OrderSolver a) =>
             Int
           -> Constraint Int a
           -> StateT (Problem a) m [a]
getNumbers evsToAssign (RightArrow f d t) = do

  -- get all from evss (except self) and all to evss
  fromEvssOther <- gets (^.. sortedEqVals.traversed.
                   filtered ((`elem` delete evsToAssign f) . view evsNr))
  toEvss <- gets (^.. sortedEqVals.traversed.filtered ((`elem` t) . view evsNr))

  -- get value of other evss
  let toNrs        = map (view value) toEvss
  let fromOtherNrs = map (view value) fromEvssOther

  -- sum values
  let toSum        = sum toNrs
  let fromOtherSum = sum fromOtherNrs

  -- calculate minimal value for this evs
  let minVal       = max 0 (toSum + d - fromOtherSum)
  return $ take numbersToTake [minVal..]

getNumbers evsToAssign (RightEqArrow f t) = do
  toEvss <- gets (^.. sortedEqVals.traversed.filtered ((`elem` t) . view evsNr))

  let toNrs = map (view value) toEvss
  let toSum = sum toNrs

  let val = toSum
  return [val]                  -- must be exactly this value

getNumbers evsToAssign (NEqArrow f t alt) = do
  -- find out before and after
  (before, after) <- getBeforeAfter evsToAssign
  evss <- gets (view sortedEqVals)

  -- transform alternatives to incorporate all derivable tuples
  let altSimp = filter (uncurry (/=)) alt
  let altOccurs = filter (\(a,b) -> a == evsToAssign || b == evsToAssign) altSimp
  let altOccursOther = map (\(a,b) -> if a == evsToAssign then b else a) altOccurs
  let altOccursOtherCombs = [(x,y)| x <- altOccursOther, y <- altOccursOther, x/=y]
  let alt' = alt ++ altOccursOtherCombs


  -- consider alternatives
  let inBeforeOrEvsToAssign x = x `elem` before || x == evsToAssign
  let getBefEvsFromSorted nr = evss ^?! traversed.filtered ((==nr) . view evsNr)
  let altValues = map (view value . getBefEvsFromSorted ***
                       view value . getBefEvsFromSorted) alt'

  let alternativesHold =
        all (\(a,b) ->  inBeforeOrEvsToAssign a && inBeforeOrEvsToAssign b) altSimp &&
        all (uncurry (==)) altValues

  let otherVal = view value $ getBefEvsFromSorted t

  if alternativesHold
    then return [0..]           -- everything allowed, as alternatives hold
    else                        -- everything, but value of t
        return $ take numbersToTake $ [0..(otherVal-1)] ++ [otherVal+1..]

getNumbers evsToAssign (MultArrow f n t) = do

  -- get ns info
  nsVal <- getNsValueUnsafe n
  nsAss <- getNsAssignmentUnsafe n
  let nsIsStrict = nsAss == Strict

  -- get to info
  toVal <- gets (^?! multipliers.traversed.filtered ((==t) . view evsNr).value)
  toValAss <- gets (^?! multipliers.traversed.filtered ((==t) . view evsNr).assignment)
  let toValIsStrict = toValAss == Strict

  let fstVal = nsVal `times` toVal
      restVals
        | nsIsStrict && toValIsStrict = []
        | nsIsStrict                 = [nsVal `times` (toVal+1), nsVal `times` (toVal+2)..]
        | toValIsStrict              = [(nsVal+1) `times` toVal, (nsVal+2) `times` toVal]
        | nsVal == 0 && toVal == 0      = [fstVal+1..] -- all others
        | nsVal == 0                  = [toVal..]    -- at least toVal
        | toVal == 0                  = [nsVal `times` (toVal+1)..] -- at least
        | otherwise                  = tail [nsVal `times` toVal..]
  return (take numbersToTake $ [fstVal] ++ restVals)

-- ^ Get a ns value by its ns name. This is unsafe at it uses fromJust!
getNsValueUnsafe      :: (Monad m) =>
                        String
                      -> StateT (Problem a) m Int
getNsValueUnsafe name = fmap snd3 (getNsTupleUnsafe name)

-- ^ Get a ns assignment by its ns name. This is unsafe at it uses fromJust!
getNsAssignmentUnsafe :: (Monad m) =>
                        String
                      -> StateT (Problem a) m Assignment
getNsAssignmentUnsafe name = fmap thd3 (getNsTupleUnsafe name)

-- ^ Get a ns tuple by its name. This is unsafe at it uses fromJust!
getNsTupleUnsafe      :: (Monad m) =>
                        String
                      -> StateT (Problem a) m (String,Int,Assignment)
getNsTupleUnsafe name = do
  nsTbl <- gets (view ns)
  let entry = fromJust $ find ((== name) . fst3) nsTbl
  return entry


-- ^ This function calculates the intersection of two sorted lists.
intersectSorted        :: (Ord a, Eq a) => [a] -> [a] -> [a]
intersectSorted [] yss = []
intersectSorted xss [] = []
intersectSorted xss@(x:xs) yss@(y:ys)
  | x < y              = intersectSorted xs yss
  | y < x              = intersectSorted xss ys
  | otherwise          = x : intersectSorted xs ys

-- ^ Given a value a calculate all possible linear combinations to get to the given
-- value.
multArrCombs :: (Enum a, OrderSolver a, Ord a, Num a) => a -> [(Int, a)]
multArrCombs 0 = [(0,x) | x <- [0..]] ++ [(x,0)| x<-[0..]]
multArrCombs newVal =
    [(i, a) | i <- reverse [0..length as], a <- as, i `times` a == newVal]
  where as = [0 .. newVal]

-- ^ This function takes in a evsNr which will be checked against the removed
-- constraints. It returns a tuple specifying a list of possible values, and a set
-- of functions which could solve the problem of a too small range of possible
-- values. This works only for RightArrows, not for RightEqArrows!
getNumbersFromRemovedConstr :: (Monad m, Ord a, Num a, Show a, Enum a) =>
                              (a -> Doc)
                            -> Int
                            -> StateT (Problem a) m ([a],[Problem a])
getNumbersFromRemovedConstr pA evsToAssign = do

  -- get some values that are needed
  (_,after) <- getBeforeAfter evsToAssign

  -- get temporary constraints for this evs
  tempRemConstr <- gets (view removedConstraints)
  let constr = filter (logAnd [ (evsToAssign `elem`) . getTos
                              , all (`elem` evsToAssign:after) . getTos
                              ]) tempRemConstr

  if null constr
    then return (take numbersToTake [0..],[]) -- all numbers possible, no need for
                                              -- solution functions
    else do
    -- check for RightEqArrow, and throw an error, as they are not handled
    let constrEqRightArr = filter isRightEqArrow constr
    unless (null constrEqRightArr)
      (error "getNumbersFromRemovedConstr works only for RightArrows")

    -- get arrows and number
    evss <- gets (^. sortedEqVals)
    let getEvsFromSorted nr = fromJust (find ((==nr) . view evsNr) evss)
    let constrEvsVals =
          map (map (view value) . map getEvsFromSorted . getFroms) constr
    let constrNum = minimum $ map minimum constrEvsVals

    let retNums = take numbersToTake [0..constrNum]

    -- create possible solution problems
    p <- get
    let possibleSolutions =
          trace ("getNumbersFromRemovedConstr " ++ show evsToAssign)
          trace ("constr: " ++ show constr)
          trace ("constrEvsVals: " ++ show constrEvsVals)
          trace ("(zip constrEvsVals constr): " ++ show (zip constrEvsVals constr))
          concatMap (\(nums,arr) ->
                     -- if length retNums == numbersToTake
                     -- then []
                     -- else
                         let froms = getFroms arr
                         in map (\f -> flip execState p $
                                      do zeroEvsNr <- trace ("bef: " ++ show (view ns p))
                                           getZeroEvsNr
                                         val <- getCurrentValue f
                                         trace ("Poss sol: " ++ show (RightArrow
                                                                      [f] (val+1) [zeroEvsNr]))
                                           insertConstraintSorted (RightArrow [f] (val+1)
                                                                   [zeroEvsNr])
                                ) froms
                    ) (zip constrEvsVals constr)

    trace ("evs " ++ show evsToAssign ++ " constrNum: " ++ show constrNum ++
           "solutions: " ++ show (length possibleSolutions))

      return (retNums,possibleSolutions)


-- ^ This function fetches the current set value of the sortedEqVals of the given
-- EqualValues (given by its evsNr).
getCurrentValue :: (Monad m) =>
                  Int
                -> StateT (Problem a) m a
getCurrentValue nr =

    gets (^?! sortedEqVals.traversed.filtered ((==nr) . view evsNr).value)


--
-- AssignNumbers.hs ends here


