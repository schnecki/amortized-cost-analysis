-- AnalyzeProblem.hs ---
--
-- Filename: AnalyzeProblem.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May 18 11:46:41 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May 18 12:34:51 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 62
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Problem.AnalyzeProblem
  ( analyzeProblemsAndImplyConstraints
  ) where

import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Ops
import           Data.OrderSolver.Constraint.Pretty
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.Data.Type
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Insert
import           Data.OrderSolver.Problem.Ops
import           Data.OrderSolver.Problem.Pretty
import           Data.OrderSolver.Problem.Type
import           Data.OrderSolver.Types
import           Data.OrderSolver.Util

import           Control.Arrow
import           Control.Lens                       hiding (from, to)
import           Control.Monad.State
import           Data.Function                      (on)
import           Data.List


import           Debug.Trace
import           Text.PrettyPrint


analyzeProblemsAndImplyConstraints :: (Eq a, Show a, Num a, Ord a) =>
                                    State [Problem a] ()
analyzeProblemsAndImplyConstraints =

  modify (concatMap (execStateT analyzeProblemAndImplyConstraints))


analyzeProblemAndImplyConstraints :: (Monad m, Eq a, Show a, Num a, Ord a) =>
                                    StateT (Problem a) m ()
analyzeProblemAndImplyConstraints = do

  implyConstraintsByLhsOfRightArrows

  findAndInsertNSConstraints


-- ^ This function looks at the EqualValues and figures out which sorted
-- EqualValues have to be >0. E.g., the instance. This must be done before sorting
-- starts.
--
-- 14 --0--> 16+17+18
-- 16 --0--> 19
-- 19+11+12 --1--> 13+14
--
-- implies that 11+12 have to be greater than 13!
implyConstraintsByLhsOfRightArrows :: (Monad m, Eq a, Show a, Num a, Ord a) =>
                                     StateT (Problem a) m ()
implyConstraintsByLhsOfRightArrows = do

  rightArrs <- gets (^.. eqVals.traversed.constraints.traversed.
                      filtered (logOr [ isRightArrow, isRightEqArrow]))

  zeroEvsNr <- getZeroEvsNr

  let startPoints = concat $ filter ((==1) . length) $ fmap getFroms rightArrs
  let getFromJust Nothing = 0
      getFromJust (Just x) = x

  let fromToDistTuples = map (getFroms &&& (getTos &&& (^? dist))) rightArrs
  let fromToDistList =
        concatMap (\(a,(x,y,z)) -> zip (repeat a) -- from
                                  (zip x (zip4   -- to
                                          (repeat (getFromJust y)) -- cost
                                          (repeat z)               -- lhs
                                          (repeat x)               -- rhs
                                          (repeat [])              -- to be filled
                                         ))) $
        concatMap (\(a,(x,y)) -> zip a (repeat (x,y,a))) fromToDistTuples
  let jumps =
        map (\xs -> (fst (head xs), -- from here to ...
                    nub $          -- (here, with cost d, all other lhs of this
                    map snd xs     -- arrow, all other rhs,[])
                    )) $
        groupBy ((==) `on` fst) $ sortBy (compare `on` fst ) fromToDistList

  let saveFun _ [] = []
      saveFun node@(to, (cst,lhss,rhss,ls)) ((to',(cst',lhss',rhss',_)):xs)
        | cst + cst' == 0     =
            (to', (cst+cst', lhss++rhss++lhss'++rhss',[],to:to':ls)) :
            saveFun node xs
        | cst == 0 && cst' > 0 =
            (to', (cst+cst', lhss++lhss',rhss',to:to':ls)) : saveFun node xs
        | otherwise          =
            (to', (cst+cst', lhss,rhss,to:to':ls)) : saveFun node xs

  -- to breath first search to find cycles
  let searches = map (bfs saveFun jumps) startPoints
  let relevantSearches = filter (\(a, xs) -> a `elem` map fst xs) searches
  let sortedOnly xs = filter (`notElem` xs)
  let addZeroIfNecassary [] = [zeroEvsNr]
      addZeroIfNecassary xs = xs
  let relevantInst =
        concatMap (\(a,xs) ->
              filter (\(x,y) -> x==a && fst4 y>0 -- && not (null (thd4 y))
                     ) $
              map (second (\(d,l,r,ns) -> (d,sortedOnly ns l,
                                          addZeroIfNecassary (sortedOnly ns r)
                                         ,sort ns)))
              xs) relevantSearches

  -- create constraints to add
  let convertToArr (d,lhs,rhs,_) = RightArrow lhs d rhs
  let nConstraints = nub $ map (convertToArr . snd) relevantInst

  unless (null nConstraints)
    (do

        trace ("nConstraints: " ++ show nConstraints)
          mapM_ insertConstraint nConstraints)


findAndInsertNSConstraints :: (Monad m, Show a, Ord a, Num a, Eq a) =>
                             StateT (Problem a) m ()
findAndInsertNSConstraints = do
  return ()


-- -- | This function does following. First, it fiends all pairs (nx,ny) with nx /=
-- -- ny, whereas nx and ny are multArrN's. Then it finds cycles such that nx >= ny +
-- -- c and ny >= nx + d. From this it follows that either c>0 or d>0 or nx==ny and
-- -- therefore all Equalvalues u and v with u=nx*A and v=ny*A have to be equal.
-- detectMultArrCycles :: (Num a, Ord a, Show a, Pretty (Problem a), Pretty (EqualValues a))
--                       => Problem a
--                       -> [(Problem a, [((Int, String), [(Int,String)])])]
-- detectMultArrCycles ord@(Problem evs _ _ _ _) =
--     -- trace ("possibleProblemEvssNrs" ++ show possibleProblemEvssNrs)
--     -- trace ("possibleProblemEvssPairs" ++ show possibleProblemEvssPairs)
--     -- trace ("pairs: " ++ show pairs)
--     -- trace ("arrsToConsider: " ++ show arrsToConsiderAsPairs)
--     trace ("relevantStartingPoints: " ++ show relevantStartingPoints)

--     -- trace ("evsNs: " ++ show evsNs)
--     -- -- trace ("hmap: " ++ show hmap)
--     trace ("searches: " ++ show searches)
--     trace ("searches': " ++ show searchesNsDeps)
--     -- trace ("replacedSearch: " ++ show replacedSearch)
--     trace ("troubles: " ++ show troubles) $
--     trace ("troublesNs: " ++ show troublesNs) $
--     -- trace ("searchPaths : "++ show (breathFirstSearchPath arrsToConsiderAsPairs 22 61) )
--     trace ("searchPaths : "++ show searchPaths ) $
--     trace ("groups: "++ show groups)
--     trace ("eqTuples: " ++ show eqTuples)
--     trace ("eqEqVal: " ++ show eqEqVal) $
--     if null searchPaths
--     then [(ord, searchesNsDeps)]
--     else
--       map (\x -> (x, searchesNsDeps)) $
--       concatMap (foldM (\acc (x,y) -> insertEqConstraintSingleNr' x y acc) ord) eqEqVal ++
--          foldM (\accOut (_,r) ->
--                  foldM (\acc (_,cs) ->
--                          insertGeqConstraint (map getVar cs) [Constant 1] acc) accOut r
--                ) ord searchPaths

--     where pairs = foldl fun [] possibleProblemEvssPairs
--           getVar x = head . variables $ getEqValsEvsNr' x ord

--           fun acc (nr1, (_,t)) =
--               let others = filter ((== t) . fst . snd) possibleProblemEvssPairs
--                   listToAdd = filter (`notElem` acc) $
--                               map (\nr2 -> case compare (length nr1) (length nr2) of
--                                             GT -> (nr2, nr1)
--                                             LT -> (nr1, nr2)
--                                             EQ -> if nr1 < nr2
--                                                  then (nr1, nr2)
--                                                  else (nr2, nr1))
--                                       (nub $ map fst others)
--               in listToAdd ++ acc


--           -- possibleProblemEvssNrs = map evsNr possibleProblemEvss
--           possibleProblemEvss = filter (logAnd [hasNEqArrow, hasMultArrow]) evs
--           possibleProblemEvssPairs =
--               concatMap ((\(mulArr, ps) -> concatMap (\m -> zip (repeat m) ps) mulArr) .
--                          (map multArrN . filter isMultArrow . arrows ***
--                           (map toPair . filter isNEqArrow . arrows)))
--                             (zip possibleProblemEvss possibleProblemEvss)
--           hasNEqArrow ev = any isNEqArrow (arrows ev)
--           hasMultArrow ev = any isMultArrow (arrows ev)

--           toPair (NEqArrow f t _) = (f, t)
--           toPair _                = error "Programming error. Should not have happend."

--           rightArrEvss = filter (logAnd [hasMultArrow, not . isCtr]) evs

--           evsNs = concatMap (\x -> zip (repeat $ evsNr x)
--                                   (map multArrN $ filter isMultArrow (arrows x))
--                             ) rightArrEvss

--           relevantStartingPoints = map evsNr rightArrEvss
--           searches = map (breathFirstSearch (arrsToConsiderAsPairs evs)) relevantStartingPoints


--           searchesNsDeps =
--             trace ("searches: " ++ show searches)
--             concatMap (filterInterestingOnes . replaceWithNsTuple) searches

--           filterInterestingOnes (x@(_, xNs),ys)
--             | null filteredYs = []
--             | otherwise = [(x,filteredYs)]
--               where filteredYs = filter ((/=xNs) . snd) ys


--           replaceWithNsTuple (x,ys) = (head $ replaceWithNs x, concatMap replaceWithNs ys)

--           replaceWithNs nr =
--             case find (isMultArrow . snd)
--                  ((\x -> zip (repeat (evsNr x)) (arrows x)) (getEqValsEvsNr' nr ord)) of
--               Nothing -> []
--               Just (nr',arr') -> [(nr',multArrN arr')]

--           replacedSearch = filter ((>1) . length . snd) $
--                            map (-- (\x -> (x, head $ replaceNumber x))
--                                 head . replaceNumber
--                                          ***
--                                 concatMap replaceNumber
--                                 -- (\x -> (x, concatMap replaceNumber x))
--                                ) searches
--           replaceNumber n = -- map snd $
--                             filter ((== n) . fst) evsNs

--           troubles = concatMap (\(a,b) ->
--                               let x = filter ((== a) . snd . fst) replacedSearch
--                                   y = filter ((== b) . snd . fst) replacedSearch
--                                   bfilter = (filter ((==b) . snd) . concatMap snd) x
--                                   afilter = (filter ((==a) . snd) . concatMap snd) y
--                               in [((map fst y, afilter), (map fst x, bfilter)) |
--                                   not (null bfilter) && not (null afilter)
--                                  ]
--                                   ) pairs

--           searchPaths = map (\((sls,els),(srs,ers)) ->
--                              (concatMap (\((s,_),(e,_)) ->
--                                  breathFirstSearchPathWithFirstLevel
--                                  (arrsToConsiderAsPairs evs) s (\_ x -> x == e))
--                              (zip sls els)
--                              , concatMap (\((s,_),(e,_)) ->
--                                  breathFirstSearchPathWithFirstLevel
--                                  (arrsToConsiderAsPairs evs) s (\_ x -> x == e))
--                              (zip srs ers)
--                              )) troubles
--           troublesNsTuples = map (\((sls,_),(srs,_)) ->
--                                       (snd (head sls), snd (head srs))) troubles

--           troublesNs = concatMap (\((sls,_),(srs,_)) ->
--                                       [snd (head sls), snd (head srs)]) troubles

--           ctrEvs = filter isCtr evs


--           groups = concatMap nub $
--                    groupBy (\x y -> if ((==) `on` thd3) x y
--                                    then ((==) `on` snd3) x y
--                                    else ((==) `on` thd3) x y)$
--                    sortBy (\x y -> if (compare `on` thd3) x y == EQ
--                                   then (compare `on` snd3) x y
--                                   else (compare `on` thd3) x y) $
--                    filter ((`elem` troublesNs) . snd3) $
--                    concatMap (\(MultArrow f n t) -> zipWith (\a b -> (a,n,b)) f t)
--                                (filter isMultArrow $ concatMap arrows ctrEvs)

--           -- nums = nub $ concatMap (map thd3) groups
--           -- eqTuples = foldl (\a pair ->
--           --                   foldl (check pair) a (zip [0..] groups)) [] troublesNsTuples
--           eqTuples = map (\pair ->
--                           foldl (check pair) [] (zip [0..] groups)) troublesNsTuples
--           eqEqVal = map (map (fst3 *** fst3)) eqTuples
--           check (x,y) acc (nr, e@(_,n,a)) = foldl check' acc (drop nr groups)
--               where check' acc' f@(_,n2,a2)
--                         | ((n == x && n2 == y) || (n == y && n2 == x)) && a == a2 = (e,f):acc'
--                         | otherwise = acc'


--
-- AnalyzeProblem.hs ends here
