-- SortEqualValues.hs ---
--
-- Filename: SortEqualValues.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Fri May  6 15:48:54 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May 18 12:24:48 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 824
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Problem.SortEqualValues
    ( sortEqualValues
    ) where

import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Ops
import           Data.OrderSolver.Constraint.Pretty
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.Data.Type
import           Data.OrderSolver.EqualValues.Ops
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Insert
import           Data.OrderSolver.Problem.AnalyzeProblem
import           Data.OrderSolver.Problem.Ops
import           Data.OrderSolver.Problem.Pretty
import           Data.OrderSolver.Problem.Type
import           Data.OrderSolver.Types
import           Data.OrderSolver.Util
import           Data.OrderSolver.Variable.Ops

import           Control.Arrow
import           Control.Lens                            hiding (from, to)
import           Control.Monad.State
import           Data.Function                           (on)
import           Data.List


import           Debug.Trace
import           Text.PrettyPrint


sortEqualValues :: (Monad m, Eq a, Show a, Num a, Ord a) =>
                StateT [Problem a] m ()
sortEqualValues = do
  -- revert sorting problem
  modify (concatMap (execStateT revertProblem))

  -- set EqualValues which have no outgoing constraint
  modify (concatMap (execStateT setFixedEvs))

  -- recursively set EqualValues which only depend on already set EqualValues
  modify (concatMap (execStateT recursivelyDependentOnAlreadySet))

  -- -- imply constraints by analyzing the leftover unsorted EqualValues
  -- modify (concatMap (execStateT implyConstraintsByUnsorted))

  -- set the unsorted (cycles)
  setUnsorted

-- ^ Completely reverts the problem to the original (except for simplifications)
-- problem, deleting the current sorted and unsorted EqualValues and replacing them
-- by the EqualValues from eqVals.
revertProblem :: (Monad m, Eq a, Show a, Num a, Ord a) =>
                StateT (Problem a) m ()
revertProblem = do
    -- revert sorting problem
  evss <- gets (view eqVals)
  sortedEqVals .= []
  unsortedEqVals .= evss

-- ^ Makes all sorted EqualValues to unsorted EqualValues.
revertSorted  :: (Monad m, Eq a, Show a, Num a, Ord a) =>
                StateT (Problem a) m ()
revertSorted = do
  sorted <- gets (view sortedEqVals)
  unsortedEqVals %= (sorted ++)
  sortedEqVals .= []

setFixedEvs :: (Monad m, Eq a, Show a, Num a, Ord a) =>
              StateT (Problem a) m ()
setFixedEvs = do
  -- set zero evs
  zeroEvsNrs <-
    gets (^.. unsortedEqVals.traversed.
          filtered (\x -> not (null (x ^.. variables.
                                     filtered (zeroVariable `elem`)))))
  -- add zeroEvs not done so
  unless (null zeroEvsNrs)
    (do zeroEvs <- getZeroEvs
        unsortedEqVals %= filter (/= zeroEvs) -- remove from unsorted
        sortedEqVals %= ([zeroEvs] ++))      -- add to sorted at end

  -- set all evs without outgoing arrow
  toSet <- gets (^.. unsortedEqVals.traversed.filtered
                (\evs -> null (evs ^.. constraints.traversed.
                              filtered (not . isMultArrow))))


  unsortedEqVals %= filter (`notElem` toSet) -- remove from unsorted
  sortedEqVals %= (toSet ++)                 -- add to sorted at end

-- implyConstraintsByUnsorted :: (Monad m, Eq a, Show a, Num a, Ord a) =>
--                              StateT (Problem a) m ()
-- implyConstraintsByUnsorted = do

--   implyConstraintsByLhsOfRightArrows


recursivelyDependentOnAlreadySet :: (Monad m, Eq a, Show a, Num a, Ord a) =>
                                   StateT (Problem a) m ()
recursivelyDependentOnAlreadySet = do

  -- set recursively on already set
  smthChanged <- recursivelyDependentOnAlreadySet'

  -- do recursion if something changed
  when smthChanged
    recursivelyDependentOnAlreadySet


recursivelyDependentOnAlreadySet' :: (Monad m, Eq a, Show a, Num a, Ord a) =>
                                   StateT (Problem a) m Bool
recursivelyDependentOnAlreadySet' = do

  sortedNrs <- gets (^.. sortedEqVals.traversed.evsNr)
  evss <- gets (^. unsortedEqVals)

  let getDeps (RightArrow f d t) = t
      getDeps (NEqArrow f t alt) = [] -- ignore NEqArrow for sorting
      getDeps (RightEqArrow f t) = t
      getDeps (MultArrow f n t ) = [] -- ignore MultArrow for sorting

  let evsDeps = map (id &&& concatMap getDeps . view constraints) evss
  let evsToMove = map fst $ filter (all (`elem` sortedNrs) . snd) evsDeps

  unsortedEqVals %= filter (`notElem` evsToMove) -- remove from unsorted
  sortedEqVals %= (evsToMove ++)                 -- add to sorted


  return (not $ null evsToMove)


setUnsorted :: (Monad m, Eq a, Show a, Num a, Ord a) =>
              StateT [Problem a] m ()
setUnsorted = do
  ps <- get
  let ps' = concatMap (evalState setUnsortedByConstraintRemoval) ps
  put ps'

  -- try to set the problem again
  modify (concatMap (execStateT setFixedEvs))
  modify (concatMap (execStateT recursivelyDependentOnAlreadySet))


-- ^ This function finds the cycles and resolves them by trying out all
-- possibilities.
setUnsortedByConstraintRemoval :: (Eq a, Show a, Num a, Ord a) =>
                            State (Problem a) [Problem a]
setUnsortedByConstraintRemoval = do

  unsorted <- gets (^. unsortedEqVals)
  let cycles = findCycles unsorted

  ps' <- mapM resolveCycle cycles
  return (concat ps')


-- ^ This function returns all possibilities of removing one of the arrows causing
-- the cycle. The removed arrow will be saved in the removedConstraints list.
resolveCycle :: (Eq a, Show a, Num a, Ord a) =>
               [Int]
             -> State (Problem a) [Problem a]
resolveCycle cycle = do

  -- get arrows from cycle
  arrs <- gets (^.. unsortedEqVals.traversed.filtered ((`elem` cycle) . view evsNr).
               constraints.traversed.filtered (\x -> any (`elem` cycle) (getTos x)))

  let arrs' =                   -- sort by length from's and then by length to's
        concatMap (sortBy (compare `on` length . getTos)) $
        groupBy ((==) `on` length . getFroms) $
        sortBy (compare `on` length . getFroms) $
        filter (isRightArrow) arrs -- only on RightArrows

  -- get current problem
  p <- get

  -- function to remove arrow from problem and set as removed
  let removeAndSet arr = do
        removeConstr arr
        removedConstraints %= (arr:)

  -- generation of return values. try out all possiblitites
  let ps = map (\a -> execState (removeAndSet a) p) arrs'

  return ps

-- ^ This function removes the given arrow from the unsortedEqVals and
-- sortedEqVals.
removeConstr        :: (Eq a, Monad m, Num a, Show a, Ord a) =>
                      Constraint Int a
                    -> StateT (Problem a) m ()
removeConstr constr = do
  sortedEqVals.traversed.constraints %= filter (/= constr)
  unsortedEqVals.traversed.constraints %= filter (/= constr)


--
-- SortEqualValues.hs ends here
