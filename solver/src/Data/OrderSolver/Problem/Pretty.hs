-- Pretty.hs ---
--
-- Filename: Pretty.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 17:14:42 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Tue May 10 11:16:30 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 69
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Problem.Pretty where

import           Data.OrderSolver.Assignment.Pretty
import           Data.OrderSolver.Constraint.Pretty
import           Data.OrderSolver.EqualValues.Pretty
import           Data.OrderSolver.Problem.Type


import           Text.PrettyPrint


prettyProblem :: (a -> Doc) -> Problem a -> Doc
prettyProblem pA (Problem sort unsort remConstr evs ctrs ns _  _ _) =
              text "Ns:" $+$ vcat (map (\(n,v,a) -> text n <> colon <> int v <>
                                                   prettyAssignment a) ns)
              $+$ text "" $+$
              text "Constructors: " $+$ vcat (map (prettyEqualValues ns pA) ctrs)
              $+$ text "" $+$
              text "EqualValues: " $+$ vcat (map (prettyEqualValues ns pA) evs)
              $+$ text "" $+$
              text "Sorted: " $+$ vcat (map (prettyEqualValues ns pA) sort)
              $+$ text "" $+$
              text "Unsorted: " $+$ vcat (map (prettyEqualValues ns pA) unsort)
              $+$ text "" $+$
              text "Temporary Removed Constraints: " $+$
              vcat (map (prettyConstraint ns pA) remConstr) $+$ text ""


--
-- Pretty.hs ends here
