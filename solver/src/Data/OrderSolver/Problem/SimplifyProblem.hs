-- SimplifyProblem.hs ---
--
-- Filename: SimplifyProblem.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Thu May  5 19:05:50 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May 18 11:48:20 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 571
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Problem.SimplifyProblem
    ( simplifyProblems
    ) where

import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Ops
import           Data.OrderSolver.Constraint.Pretty
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.Data.Type
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Insert
import           Data.OrderSolver.Problem.Ops
import           Data.OrderSolver.Problem.Pretty
import           Data.OrderSolver.Problem.Type
import           Data.OrderSolver.Types
import           Data.OrderSolver.Util

import           Control.Arrow
import           Control.Lens                       hiding (from, to)
import           Control.Monad.State
import           Data.Function                      (on)
import           Data.List


import           Debug.Trace
import           Text.PrettyPrint


simplifyProblems :: (Show a, Ord a, Num a, Eq a) => State [Problem a] ()
simplifyProblems = do

  -- merge eqVals with constraint < zeroEvs
  modify (concatMap (execStateT mergeEvsSmallerThanZero))

  -- merge eqVals with same rhs of MultArrow, e.g. 38=n9*64, 62=n9*64 --> 38==62
  modify (concatMap (execStateT mergeEvsWithSameMultArrRhs))

  -- simplify constraints itself as far as possible
  modify (concatMap (execStateT simplifyConstraints))

  -- merge eqVals with no arrows to zeroEvs
  modify (concatMap (execStateT mergeEvsWithNoConstraints))

  -- merge cycles with x --0--> y and y --0--> x of arbitrary length
  modify (concatMap (execStateT mergeCyclesWithZeroCumulativeCost))

  -- remove unsolvable problems
  -- modify (concatMap (execStateT deleteInfeasible))

  -- merge cycles which have to be 0, e.g. from the path 14 -0-> 16+17+18 -1->
  -- 13+14 follows that 16,17,18,14 must be 0
  -- modify (concatMap (execStateT mergeCycleEvsWichHaveToBeZero))

  -- detect unsolvable cycles omitted by MultArrows and merge eqVals if otherwise a
  -- problem would occur
  -- modify (concat . concatMap (evalStateT resolveMultArrCycleConstants))

  -- -- final simplification of constraints
  modify (concatMap (execStateT simplifyConstraints))

mergeEvsSmallerThanZero :: (Show a, Ord a, Num a, Eq a, Monad m) =>
                             StateT (Problem a) m ()
mergeEvsSmallerThanZero = do
  zeroEvsNr <- getZeroEvsNr

  constr <- gets (^.. eqVals.traversed.constraints.traversed.filtered isRightArrow.
                 filtered isRightArrow.
                 filtered (logAnd [ (==1) . length . (^?! from)
                                  , (==1) . length . (^?! to)
                                  , (==zeroEvsNr)  . head . (^?! to)]))

  when (any ((>0) . (^?! dist)) constr)
    (fail "values must be >= 0")

  let merges = map (head . (^?! from)) constr
  mapM_ (mergeEvs zeroEvsNr) merges


mergeEvsWithSameMultArrRhs :: (Show a, Ord a, Num a, Eq a, Monad m) =>
                             StateT (Problem a) m ()
mergeEvsWithSameMultArrRhs = do

  -- all EqualValues with MultArrs
  multArrs <- gets (^.. eqVals.traversed.constraints.traversed.filtered isMultArrow)

  let multArrRhsTupel (MultArrow _ n t) = (n,t)
      multArrRhsTupel _                 = error "Should not happen."
  let grouped =
        map (map (^?! multArrFrom)) $
        filter ((> 1) . length) $
        groupBy ((==) `on` multArrRhsTupel) $
        sortBy (compare `on` multArrRhsTupel) multArrs
  let tuples = concatMap (\xs -> zip (repeat $ head xs) (tail xs)) grouped
  let repl = getReplacementListFromTuples tuples

  trace ("grouped: " ++ show grouped)
    trace ("repl: " ++ show repl)
    mapM_ (uncurry mergeEvs) repl

-- ^ This function simplifies all constraints in eqVals using the function
-- simplifyConstraintEvs.
simplifyConstraints :: (Monad m, Show a, Ord a, Num a, Eq a) =>
                      StateT (Problem a) m ()
simplifyConstraints = do
  evss <- gets (view eqVals)
  mapM_ simplifyConstraintEvs evss

-- ^ This function simplifies all constraints in the given EqualValues.
simplifyConstraintEvs :: (Monad m, Show a, Ord a, Num a, Eq a) =>
                        EqualValues a ->
                        StateT (Problem a) m ()
simplifyConstraintEvs evs = do
  zeroEvsNr <- getZeroEvsNr
  let nr = view evsNr evs

  let simplifyArr (RightArrow f d t)
       | null f' && null t' && d > 0               =
           fail $ "cannot fulfill: " ++ show (RightArrow f d t)
       | nr `notElem`  (f' ++ t')                = return []
       | (null f' && null t') || (null t' && d == 0) = return []
       | null t'                                 =
           return [RightArrow f' d [zeroEvsNr]]
       | otherwise                               = return [RightArrow f' d t']
       where f' = f \\ t
             t' = t \\ f
      simplifyArr arr@(RightEqArrow f [t])
       | f == t                                   = return []
       | otherwise                               = return [arr]
      simplifyArr (RightEqArrow f t) =
        return [RightEqArrow f (if null rest then [zeroEvsNr] else rest)]
       where rest = filter (/= zeroEvsNr) t
      simplifyArr (NEqArrow f t alt)
       | all (uncurry (==)) alt                   = return []
       | all (\(a,b) -> (a == f && b == t) || (a == t && b == f)) alt = return []
       | otherwise                               =
           return [NEqArrow f t (filter (uncurry (/=)) alt)]
      simplifyArr x                              = return [x]

  -- simplify Constraints, one after the other
  arrs <- mapM simplifyArr (view constraints evs)
  modify (eqVals.traversed.filtered (\e -> view evsNr e == view evsNr evs).
                    constraints .~ (concat $ nub arrs))

-- ^ This function merges all EqVals which have no constraints (and thus can be set
-- to 0) with the zeroEvs.
mergeEvsWithNoConstraints :: (Monad m, Eq a, Show a, Num a) =>
                            StateT (Problem a) m ()
mergeEvsWithNoConstraints = do
  zeroEvsNr <- getZeroEvsNr
  toMerge <- gets (^.. eqVals.traversed.
                  filtered (\x -> null (view constraints x)).evsNr)

  mapM_ (mergeEvs zeroEvsNr) toMerge

-- ^ This function merges cycles, that have 0 accumulative cost and are composed of
-- RightArrow and RightEqArrow constraints with one variable on each side.
mergeCyclesWithZeroCumulativeCost :: (Monad m, Eq a, Show a, Num a) =>
                                    StateT (Problem a) m ()
mergeCyclesWithZeroCumulativeCost = do

  -- get constraints which are important for this analysis
  let filterRightArr (RightArrow f d t) = d == 0 && length f == 1 && length t == 1
      filterRightArr (RightEqArrow _ t) = length t == 1
      filterRightArr _                  = error "should not be possible"
  rightArrs <- gets (^.. eqVals.traversed.constraints.traversed.
                    filtered (logOr [isRightArrow, isRightEqArrow]).
                    filtered filterRightArr)

  -- retrieve the tuples to merge
  let jumpsD0 =
        map (\xs -> (fst (head xs), nub $ map snd xs)) $
        groupBy ((==) `on` fst) $
        sortBy (compare `on` fst) $
        map (head . getFroms &&& head . getTos) rightArrs
      searchesD0 = map (breathFirstSearchWoStart jumpsD0) (map fst jumpsD0)
      toMergeD0 =
        getReplacementListFromTuples $ concat $
        concatMap (\(x,xs) -> map (\y -> if x == y || x > y
                                       then []
                                       else [(x,y)]) xs) $
        filter (\(x,xs) -> x `elem` xs) searchesD0

  -- merge evss
  mapM_ (uncurry mergeEvs) toMergeD0


-- deleteInfeasible :: (Monad m, Eq a, Ord a, Show a, Num a) =>
--                                     StateT (Problem a) m ()
-- deleteInfeasible = do
--   rightArrs <- gets (^.. eqVals.traversed.constraints.traversed.
--                       filtered (logOr [ isRightArrow, isRightEqArrow]))
--   let startPoints = concat $ filter ((==1) . length) $ fmap getFroms rightArrs
--   let getFromJust Nothing = 0
--       getFromJust (Just x) = x
--   let fromToDistTuples = map ((^?! from) &&& ((^?! to) &&& (^? dist))) rightArrs
--   let fromToDistList =
--         concatMap (\(a,(x,y)) -> zip (repeat a) (zip x (repeat (getFromJust y)))) $
--         concatMap (\(a,b) -> zip a (repeat b) ) fromToDistTuples
--   let jumps =
--         map (\xs -> (fst (head xs), -- from here to ...
--                     map (maximumBy (compare `on` snd)) $ -- the maximum distance
--                     groupBy ((==) `on` fst) $             -- of all to's
--                     sortBy (compare `on` fst) $
--                     map snd xs)) $
--         groupBy ((==) `on` fst) $ sortBy (compare `on` fst ) fromToDistList


--   let searches = map (breathFirstSearchMaxCostWoStart jumps) startPoints
--   let filteredSeaches = filter (\(x,xs) ->
--                                  case find ((== x) . fst) xs of
--                                    Nothing -> False
--                                    Just (_,d) -> d > 0) searches

--   trace ("startPoints: " ++ show startPoints)
--     trace ("fromToDistTuples: " ++ show fromToDistTuples)
--     trace ("fromToDistList: " ++ show fromToDistList)
--     trace ("jumps: " ++ show jumps)
--     trace ("searches: " ++ show searches)
--     trace ("filteredSeaches: " ++ show filteredSeaches) undefined
    -- error they are not infeasible!!!!

--     undefined


-- resolveMultArrCycleConstants :: (Monad m) => StateT (Problem a) m [Problem a]
-- resolveMultArrCycleConstants = do
--   -- get all pairs with nx /= ny
--   multArrsEvsNrs <- gets (^.. eqVals.traversed.constraints.traversed.
--                          filtered isMultArrow.multArrFrom)
--   nEqArrs <-  gets (^.. eqVals.traversed.constraints.traversed.filtered isNEqArrow)

--   let nEqTuples =
--         nub $ map (\(a,b) -> if a < b then (a,b) else (b,a)) $
--         map ((^?! nEqFrom) &&& (^?! nEqTo)) nEqArrs

--   trace ("nEqTuples: " ++ show nEqTuples)
--     undefined
--   where arrsToConsiderAsPairs :: Foldable t => t (EqualValues t1) -> [(Int, [Int])]
--         arrsToConsiderAsPairs evs =
--           concatMap convertToTuples $ filter (logOr [isRightArrow, isRightEqArrow]) arrs
--           where arrs                               = concatMap (view constraints) evs
--                 convertToTuples (RightArrow f _ t) = zip f (repeat t)
--                 convertToTuples (RightEqArrow f t) = [(f, t)]
--                 convertToTuples _                  = error "should not be possible"


--
-- SimplifyProblem.hs ends here
