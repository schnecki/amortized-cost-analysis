{-# LANGUAGE TemplateHaskell #-}
-- Type.hs ---
--
-- Filename: Type.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:36:21 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Sun May  8 23:01:22 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 79
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Problem.Type where

import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Variable.Type

import           Control.Lens                      hiding (Strict, empty)

data Problem a = Problem
                 { _sortedEqVals       :: [EqualValues a]
                 , _unsortedEqVals     :: [EqualValues a]
                 , _removedConstraints :: [Constraint Int a]
                 , _eqVals             :: [EqualValues a]
                 , _multipliers        :: [EqualValues a]
                 , _ns                 :: [(String, Int, Assignment)]
                 , _renamesEvs         :: [(Int, Int)]      -- [(nrBefore, nrAfter)]
                 , _renamesNs          :: [(String, String)] -- [(nameBefore, nameAfter)]
                 , _eqValsCounter      :: Int
                                    -- , _sorted        :: Bool
                 } deriving (Show,Read)
makeLenses ''Problem


empty :: Num a => Problem a
empty = Problem [] [] [] [EqualValues 0 [Constant 0] 0 [] Strict] [] [] [] [] 1


--
-- Type.hs ends here
