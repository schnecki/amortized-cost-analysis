-- Pretty.hs ---
--
-- Filename: Pretty.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:29:05 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fri May  6 11:02:11 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 12
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Assignment.Pretty where

import           Data.OrderSolver.Assignment.Type

import           Text.PrettyPrint

prettyAssignment :: Assignment -> Doc
prettyAssignment ass =
  case ass of
    Strict  -> text "!"
    Loosely -> text "~"
    _       -> empty


-- instance Pretty Assignment where
--   pretty Strict     = text "Strict"
--   pretty Unassigned = text "Unassigned"
--   pretty Loosely    = text "Loosely"

--
-- Pretty.hs ends here
