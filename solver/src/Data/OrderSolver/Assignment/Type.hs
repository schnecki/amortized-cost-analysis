-- Type.hs ---
--
-- Filename: Type.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:29:59 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mon May  9 23:39:53 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 12
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Assignment.Type where


data Assignment = Strict        -- ^ do not change the value
                | Unassigned    -- ^ no assignment made yet (set to default)
                | Loosely       -- ^ assigned, but may be changed w.r.t. constraints
                deriving (Show, Eq, Read)


--
-- Type.hs ends here
