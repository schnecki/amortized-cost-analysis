-- Ops.hs ---
--
-- Filename: Ops.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:33:51 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mon May  9 20:07:50 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 12
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Assignment.Ops where

import           Data.OrderSolver.Assignment.Type

isAssigned            :: Assignment -> Bool
isAssigned Strict     = True
isAssigned Loosely    = True
isAssigned Unassigned = False

mergeAssignments              :: Assignment -> Assignment -> Assignment
mergeAssignments Unassigned x = x
mergeAssignments x Unassigned = x
mergeAssignments Loosely x    = x
mergeAssignments x Loosely    = x
mergeAssignments _ _          = Strict

--
-- Ops.hs ends here
