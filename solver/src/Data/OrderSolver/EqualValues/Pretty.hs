-- Pretty.hs ---
--
-- Filename: Pretty.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 17:15:47 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May 11 18:42:31 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 27
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.EqualValues.Pretty where

import           Data.OrderSolver.Assignment.Pretty
import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Pretty
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Variable.Pretty

import           Text.PrettyPrint


prettyEqualValues :: [(String, Int, Assignment)]
                  -> (a -> Doc)
                  -> EqualValues a -> Doc
prettyEqualValues ns pA (EqualValues no vs v arrs ass) =

  int no <> text ":" <+> pA v <> prettyAssignment ass
  <+> text ":" <+> hcat (punctuate (comma <> space) (map (prettyVariable pA) vs)) <+>
  text "Arrs: " <> hcat (punctuate (comma <> space) (map (prettyConstraint ns pA) arrs))


--
-- Pretty.hs ends here
