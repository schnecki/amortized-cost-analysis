{-# LANGUAGE TemplateHaskell #-}
-- Type.hs ---
--
-- Filename: Type.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:30:53 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Fri May  6 16:20:05 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 30
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:


module Data.OrderSolver.EqualValues.Type where

import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.Variable.Type

import           Control.Lens

data EqualValues a = EqualValues
                       { _evsNr       :: Int
                       , _variables   :: [Variable a]
                       , _value       :: a
                       , _constraints :: [Constraint Int a]
                       , _assignment  :: Assignment
                       }
                       deriving (Show,Read)
makeLenses ''EqualValues

instance Eq a => Eq (EqualValues a) where
  (EqualValues nr1 _ _ _ _) == (EqualValues nr2 _ _ _ _) = nr1 == nr2


instance Ord a => Ord (EqualValues a) where
  compare (EqualValues _ vs1 _ _ _) (EqualValues _ vs2 _ _ _) = compare vs1 vs2


--
-- Type.hs ends here
