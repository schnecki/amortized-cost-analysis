-- Ops.hs ---
--
-- Filename: Ops.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:34:30 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Sun May  8 22:57:23 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 137
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.EqualValues.Ops where

import           Data.OrderSolver.Assignment.Type
import           Data.OrderSolver.Constraint.Ops
import           Data.OrderSolver.Constraint.Type
import           Data.OrderSolver.EqualValues.Type
import           Data.OrderSolver.Util


import           Control.Arrow
import           Control.Lens                      hiding (Strict)
import           Data.Function                     (on)
import           Data.List
import           Data.Maybe


import           Debug.Trace

isStrict :: EqualValues a -> Bool
isStrict (EqualValues _ _ _ _ Strict) = True
isStrict _ = False

isSet                               :: EqualValues a -> Bool
isSet (EqualValues _ _ _ _ Strict)  = True
isSet (EqualValues _ _ _ _ Loosely) = True
isSet _                             = False


-- ^ This function checks weather two EqualValues are strictly set two different
-- values.
strictlySetToDifferentValues :: (Eq a) =>
                               EqualValues a
                             -> EqualValues a
                             -> Bool
strictlySetToDifferentValues evs1 evs2
  | view assignment evs1 == Strict && view assignment evs2 == Strict =
      view value evs1 /= view value evs2
  | otherwise                                                     = False


-- ^ This function checks weather the two EqualValues contain each other in a
-- NEqArrow.
notEqualValues :: (Eq a) => EqualValues a -> EqualValues a -> Bool
notEqualValues evs1 evs2 =
  (view evsNr evs2) `elem` notEqEvss

  where notEqEvss = evs1 ^.. constraints.traversed.filtered isNEqArrow.
          filtered (not . eqAndNotEq).nEqTo

        eqAndNotEq (NEqArrow f t alt) =
          let filterAlt = filter (not . uncurry (==)) alt
              (l, r) = head filterAlt
          in length filterAlt == 1 && ((f == l && t == r) || (f == r && t == l))
        eqAndNotEq _ = error "should not happen."

findCycles :: (Show a, Eq a) => [EqualValues a] -> [[Int]]
findCycles evss =
  map head $ group $
  map (\(x,xs) -> sort (x:xs)) $
  concatMap (\(a,b) -> filter ((==a) . fst) b) $
  map (bfs saveFun jumps) nrs

  where nrs                     = map (view evsNr) evss
        arrs                    = concatMap (view constraints) evss
        multArrs                = filter (logOr [isRightArrow, isRightEqArrow]) arrs

        arrTuples :: [([Int],([Int],[Int]))]
        arrTuples               = map (getFroms &&& (getTos &&& const [])) arrs

        arrTuples' =
          concatMap (\(a,(b,c)) -> zip (repeat a) (zip b (repeat c))) $
          concatMap (\(a,b) -> zip a (repeat b)) arrTuples

        jumps :: [(Int, [(Int, [Int])])]
        jumps                   =
          map (\xs -> ( fst (head xs)
                     , nub $ map snd xs)) $
          groupBy ((==) `on` fst) $ sortBy (compare `on` fst ) arrTuples'

        saveFun                 :: (Int,[Int]) -> [(Int,[Int])] -> [(Int, [Int])]
        saveFun node@(to,ls) xs = map (second (const (to:ls))) xs

--
-- Ops.hs ends here
