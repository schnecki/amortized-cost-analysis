-- Type.hs ---
--
-- Filename: Type.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:20:04 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Thu May  5 21:41:11 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 10
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Variable.Type where

data Variable a = Variable String
                | Constant a
                deriving (Show, Eq, Ord, Read)


--
-- Type.hs ends here
