-- Ops.hs ---
--
-- Filename: Ops.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:39:49 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mon May  9 19:40:52 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 20
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Variable.Ops where

import           Data.OrderSolver.Variable.Type

isVariable :: Variable a -> Bool
isVariable Variable{} = True
isVariable _ = False

getVariable :: (Monad m) => Variable a -> m String
getVariable (Variable x) = return x
getVariable _ = fail "not a variable"

getVariable' :: Variable a -> String
getVariable' (Variable x) = x
getVariable' _ = error "getVariable called on a constant"

isConstant :: Variable a -> Bool
isConstant Constant{} = True
isConstant _ = False

getConstant :: (Monad m) => Variable a -> m a
getConstant (Constant a) = return a
getConstant _ = fail "not a constant"

getConstant' :: Variable a -> a
getConstant' (Constant a) = a
getConstant' _ = error "getConstant called on a variable"

getVariableName    :: (Show a) => Variable a -> String
getVariableName var
  | isVariable var = getVariable' var
  | isConstant var = show $ getConstant' var
  | otherwise      = error "unknown variable type"


zeroVariable :: Num a => Variable a
zeroVariable = Constant 0


--
-- Ops.hs ends here
