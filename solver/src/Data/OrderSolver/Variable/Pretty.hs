-- Pretty.hs ---
--
-- Filename: Pretty.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Wed May  4 10:20:32 2016 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed May  4 20:33:07 2016 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 12
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
--
--
--

-- Code:

module Data.OrderSolver.Variable.Pretty where

import           Data.OrderSolver.Variable.Type

import           Text.PrettyPrint

prettyVariable                   :: (a -> Doc) -> Variable a -> Doc
prettyVariable pA (Variable str) = text str
prettyVariable pA (Constant x)   = pA x


--
-- Pretty.hs ends here
