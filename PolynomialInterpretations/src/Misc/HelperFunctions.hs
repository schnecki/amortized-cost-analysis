-- HelperFunctions.hs ---
--
-- Filename: HelperFunctions.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: So Dez  8 17:56:00 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:48 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 25
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:


module Misc.HelperFunctions where

-----------------------------------------------------------------------------
-- HELPER FUNCTIONS

-- | This function shows a list and appends a serperator in the middle
-- of two elements. There will be no seperator before the first element and
-- after the last element.
showListWithSep :: Show a => (a -> String) -> [a] -> String -> String
showListWithSep _ [] _       = []
showListWithSep f (x:[]) _   = f x
showListWithSep f (x:xs) sep = f x ++ sep ++ showListWithSep f xs sep


--
-- HelperFunctions.hs ends here
