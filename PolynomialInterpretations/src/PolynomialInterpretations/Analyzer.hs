{-# LANGUAGE CPP #-}
-- Analyzer.hs ---
--
-- Filename: Analyzer.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mi Feb 26 19:55:37 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:47 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 199
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:

module PolynomialInterpretations.Analyzer
    ( generatePolyInterpretations
    , createPolyInterpretationsForRR
    ) where


import           Types.TypeDatatype
import           Types.TypeGeneral
import           Types.TypePolynomialInterpretation
import           Types.TypeTypedTRS

import           Exceptions.ExceptionHandler
import           PolynomialInterpretations.PolynomialInterpretation

import           Control.Exception                                  (throw)
import qualified Data.Map.Strict                                    as Map
#ifdef DEBUG
import           Debug.Trace                                        (trace)
#endif


generatePolyInterpretations     :: TypedTRS -> [(String, Interpretation)]
generatePolyInterpretations trs = constrInterp ++ rrInterp
    where
      -- Polynomial Interpretations of constructors where the string
      -- is the constructor name.
      constrInterp :: [(String, Interpretation)]
      constrInterp = map ((\(a,b) ->
                           (a, insertCost b $ createPolyInterpretation trs
                             False False permuteLinear (getCStatement b))) .
                          (\a -> (ctrName $ getCStatement a, a)))
                     ctrs
          where
            ctrName                :: Statement -> String
            ctrName (Function n _) = n
            ctrName (Atom iv)      = getIVVariable iv

            insertCost         :: Constructor -> Interpretation -> Interpretation
            insertCost ctr inp =
                case getCCost ctr of
                  CostInt i -> case inp of
                                 InterpNode list [[Monom (a, iv, c)]] ->
                                     InterpNode list [[Monom (a, InternalVariable (getIVVariable iv)
                                                               NIL [CostInt i], c)]]
                                 InterpLeaf (Monom (a, b, c)) ->
                                     InterpLeaf (Monom (a, InternalVariable (getIVVariable b) NIL [CostInt i], c))
                                 _ -> throw $ FatalException $ "Error, more than 1 constant per " ++
                                      "interpretation is not supported."
                  _ -> inp

      -- Polynomial Interpretations for Rewrite rules, with the
      -- string being the rewrite rule name.
      rrInterp :: [(String, Interpretation)]
      rrInterp = map ((\(a,b) -> (a, createPolyInterpretation trs False True permuteLinear b)) .
                      (\a -> (getName a, Function (getName a) (take (minChldLen a)
                              (map Atom (getParTypes $ getSignature a))))))
                 rrs


      minChldLen                      :: RewriteRule -> Int
      minChldLen (RewriteRule n _ []) = throw $ FatalException $ "Signature of " ++ n ++ " has no " ++
                                        "corresponding rewrite rules given."
      minChldLen (RewriteRule _ _ rels) = minimum (map (length . getChilds . getLeft) rels)


      ctrs :: [Constructor]
      ctrs = concatMap getCConstructors $ getConstructors trs

      rrs :: [RewriteRule]
      rrs = getRewriteRules trs


createPolyInterpretationsForRR            :: TypedTRS -> [RewriteRule] -> Map.Map String Interpretation ->
                                             [RewriteRule]
createPolyInterpretationsForRR trs rrs interp =
    foldl (\acc x -> acc ++
                     [x { getRelations = map (genPolyInterpFromRel trs (dt x) (cst x) interp) $
                          zip (replicate (length $ getRelations x) $ getName x ++ "_")
                                  (getRelations x) } ])
              [] rrs

        where
          dt    :: RewriteRule -> Datatype
          dt rr = getIVDatatype $ getRetTypes $ getSignature rr
          cst    :: RewriteRule -> [Cost]
          cst rr = getIVCosts $ getRetTypes $ getSignature rr


genPolyInterpFromRel                    :: TypedTRS -> Datatype -> [Cost] ->
                                           Map.Map String Interpretation ->
                                           (String, Relation) -> Relation
genPolyInterpFromRel trs dt cst interp (prefix, Relation l _ r _) = Relation l li r ri
        where
          li = Just $ convertStmtToPolyInterpretationLhs trs (prefix -- ++ "lhs_"
                                                             ) interp l
          ri = Just $ convertStmtToPolyInterpretationRhs trs (prefix -- ++ "rhs_"
                                                             ) interp dt cst r


--
-- Analyzer.hs ends here
