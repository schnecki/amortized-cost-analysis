-- TypeSExpression.hs ---
--
-- Filename: TypeSExpression.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Di Mär 11 17:42:47 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:46 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 29
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:

module Types.TypeSExpression where


data Operator = Eq | Gt | Lt | Geq | Leq | Times | Plus | Minus | Exp

instance Show Operator where
    show Eq    = "="
    show Gt    = ">"
    show Geq   = ">="
    show Lt    = "<"
    show Leq   = "<="
    show Times = "*"
    show Plus  = "+"
    show Minus = "-"
    show Exp   = "^"


data SExpression = SNode Operator SExpression SExpression
                 | SLeaf String


instance Show SExpression where
    show (SNode o s1 s2) = "(" ++ show o ++ " " ++ show s1 ++ " " ++ show s2 ++ ")"
    show (SLeaf str)     = str


showSExpressionAsLP                 :: SExpression -> String
showSExpressionAsLP (SLeaf x)       = x
showSExpressionAsLP (SNode o s1 s2) = showSExpressionAsLP s1 ++ " " ++
                                      case o of
                                        Minus -> show Plus ++ " " ++ showSExpressionAsLP' s2
                                        Gt -> show Geq ++ " " ++ showSExpressionAsLP s2 ++ " + 1"
                                        _ -> show o ++ " " ++ showSExpressionAsLP s2
    where
      showSExpressionAsLP' (SLeaf x)       = '-' : x
      showSExpressionAsLP' (SNode o s1 s2) = showSExpressionAsLP' s1 ++ " " ++ show o ++ " " ++
                                             case o of
                                               Minus -> showSExpressionAsLP s2
                                               Gt -> show Geq ++ " " ++ showSExpressionAsLP' s2 ++ " + 1"
                                               _ -> showSExpressionAsLP' s2

--
-- TypeSExpression.hs ends here
