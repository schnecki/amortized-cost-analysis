-- TypeConstraint.hs ---
--
-- Filename: TypeConstraint.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Sa Dez  7 16:24:08 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:46 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 132
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This file holds the types for the constraints.
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:


module Types.TypeConstraint where


import           Types.TypeTypedTRS

import           Misc.HelperFunctions

data LPProblem = LPProblem
    { getPTRS         :: TypedTRS
    , getPConstraints :: [Constraint]
    , getPVarNr       :: Int
    } -- deriving (Show, Eq)


-- | This data definition is reposible for the atomic types in the constraints.
--   There are two possiblilities: An Integer alone, e.g. 4, or a variable
--   including a factor, e.g. 2 * x.
data CFact = CVar Int String
           | CConst Int
             deriving (Eq)


instance Show CFact where
    show (CConst x) = show x
    show (CVar n x)
        | n == 1    = x
        | n == (-1) = "-" ++ x ++ ""
        | otherwise = show n ++ " " ++ x


-----------------------------------------------------------------------------
-- | This data type defines a constraint. The lists are read as a sum
--   of input. E.g. CEq [x, 2, 4] [y, 3] is therefore: x + 2 + 4 = y + 3
--
-- CLeq: less or equal to
-- CEq : equal to
-- CGeq: greater or equal to
data Constraint = CLeq [CFact] [CFact]
                | CEq  [CFact] [CFact]
                | CGeq [CFact] [CFact]
                  deriving (Eq)

-- | Show instance of Constraint. Print with using showListWithSep from the module
--   HelperFunctions
instance Show Constraint where
    show constr = case constr of
                    CLeq l1 l2 -> showListWithSep show l1 " + " ++ " <= " ++
                                  showListWithSep show l2 " + "
                    CEq l1 l2  -> showListWithSep show l1 " + " ++ " = " ++
                                  showListWithSep show l2 " + "
                    CGeq l1 l2 -> showListWithSep show l1 " + " ++ " >= " ++
                                  showListWithSep show l2 " + "


--
-- TypeConstraint.hs ends here
