-- TypeDatatype.hs ---
--
-- Filename: TypeDatatype.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 23:09:50 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:46 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 63
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module holds all supported data types.
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:


module Types.TypeDatatype where


-----------------------------------------------------------------------------
-- | Any data type can be added here. The parser will automatically know them
-- using the Read instance automatically derived.
data Datatype = Datatype {
      getDatatypeString :: String
    } | NIL                        -- constructor NIL cannot be parsed!
    deriving (Read, Eq)


instance Show Datatype where
    show (NIL)        = "NIL"
    show (Datatype x) = x


--
-- TypeDatatype.hs ends here
