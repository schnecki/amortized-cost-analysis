-- TypePolynomialInterpretation.hs ---
--
-- Filename: TypePolynomialInterpretation.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mi Feb 26 19:43:59 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:46 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 103
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:

module Types.TypePolynomialInterpretation where

import           Misc.HelperFunctions
import           Types.TypeDatatype
import           Types.TypeGeneral

-- | This data type holds a variable in the following way: The first
-- integeger descrbes the number of occurences, the String stands for
-- the variables name and the second variable stands for its
-- exponential part. Example:
--
-- 4x² is represented by: Var (4, "x", 2)
data Monom = Monom (Int, InternalVariable, Int)
          -- deriving (Show)

instance Eq Monom where
 (==) (Monom (a, b, c)) (Monom (d, e, f)) =
     a == d && c == f && getIVVariable b == getIVVariable e && getIVDatatype b == getIVDatatype e

instance Show Monom where
    show (Monom (1,b,1)) = show b
    show (Monom (a,b,1)) = show a ++ "·" ++ show b
    show (Monom (1,b,c)) = show b ++ "^" ++ show c
    show (Monom (a,b,c)) = show a ++ "·" ++ show b ++ "^" ++ show c


-- | This represents a term (e.g. e*x + d + d*e) as a Node and Leaf:
-- InterpNode [([Monom (1,e,1)], [(InterpLeaf (Monom (1,x,1)))])]
--           [[Monom (1,d,1)], [Monom (1,d,1), Monom (1,e,1)]]
data Interpretation = InterpNode [([Monom], [Interpretation])] [[Monom]]
                    | InterpLeaf Monom | InterpParam Int Datatype Cost


instance Show Interpretation where
    show (InterpLeaf x)       = show x
    show (InterpParam x dt c) = 'p' : show x -- ++ " : " ++ show dt ++ " (" ++ show c ++ ")"
    show (InterpNode [] vars) =
        showListWithSep id (map (\a -> showListWithSep show a " · ") vars) " + "
    show (InterpNode (h:list) vars) = -- "(" ++
        foldl (\acc x-> acc ++ show x ++ " · ") [] (fst h) ++
        (if isLeaf then "[" else "(") ++
        showListWithSep show (snd h) " · " ++
        (if isLeaf then "]" else ")") ++
        (if null list && null vars then "" else
             " + " ++ show (InterpNode list vars) -- ++ ")"
        )
        where
          isLeaf = all (\x -> case x of
                                InterpLeaf _ -> True
                                InterpParam _ _ _ -> True
                                _ -> False) (snd h)

-- | This data type holds two list of lists. The first list is a list
-- of multiplications with variables. The second list is a list of
-- non-variable multiplications (where variable refers to a unkown
-- interpretation variable like the x in [s(x)] = x + 1, the 1 will be
-- a constant and is therefore listed in the second list).
data PolynomialInterpretation = PolyInterp
    { getMultPart  :: [[Monom]]
    , getConstPart :: [[Monom]]
    }


instance Show PolynomialInterpretation where
    show (PolyInterp v0 v1) = "\t" ++
        showListWithSep id  (map (\a -> showListWithSep show a " · ") v0) " + " ++
        if null v1 then "" else
            " + " ++
        showListWithSep id  (map (\a -> showListWithSep show a " · ") v1) " + "


--
-- TypePolynomialInterpretation.hs ends here
