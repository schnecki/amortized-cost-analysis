{-# LANGUAGE CPP #-}
-- ArgumentOptions.hs ---
--
-- Filename: ArgumentOptions.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 21:00:52 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Wed Oct  1 11:57:48 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 635
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:


module Parsing.Arguments.ArgumentOptionsParser
    ( parseArgOpts )
    where


import           Exceptions.ExceptionHandler
import           Types.TypeArgumentOptions

import           Control.Exception.Base      (throw)
import           Data.Foldable               (foldlM)
import           System.Console.GetOpt
import           System.Directory
import           System.Environment          (getArgs, getProgName)

-- |These are the default options. They are used, if the
-- options do not get set specifically.
defaultOptions :: ArgumentOptions
defaultOptions = ArgumentOptions {
                   getFilePath = ""
                 , getVerbose =  False
                 , getMaxVariable = 10000
                 , getDebug = False
                 , getMaxTreeHeight = 100
                 , getHelpText = False
                 , getCostConstraints = False
                 , getSMTSolver = False
                 , getKeepFiles = False
                 , getTempDir = "/tmp"
                 , getUseRelaxRule = False
                 }

-- |This function defines the options, the function to be called, when
-- the option is set and its help text, in case the -h option gets passed.
--
-- Arguments: The help message to be displayed in case the -h option is set.
options :: [OptDescr (ArgumentOptions -> IO ArgumentOptions)]
options =
  [ Option ['c'] ["cost-constraints"]
   (NoArg (\opts -> return $ opts { getCostConstraints = True } ))
   "Enable cost constraints. If set, for all rules the value --p--> will be > 0."

  , Option ['d'] ["temp-dir"]
   (ReqArg (\str opts -> return $ opts { getTempDir = str } ) "DIR" )
   "the temporary directory [system setting (e.g. /tmp)]"

  , Option [] ["debug"]
   (NoArg (\opts -> return $ opts { getDebug = True } ))
   "enable debug output (works only if compiled with DEBUG constant)"

  , Option ['h'] ["help"]
   (NoArg (\opts -> return $ opts { getHelpText = True } ))
   "print usage information"

  , Option ['k'] ["keep-lp-files"]
   (NoArg (\opts -> return $ opts { getKeepFiles = True } ))
   "keep the files produced by the lp solver."

  , Option ['m'] ["max-variables"]
   (ReqArg (\str opts -> do
              let readRes = reads str :: [(Int, String)]
              if null readRes
              then return (opts { getMaxVariable = -1 })
              else do
                let [(nr, _)] = readRes
                return $ opts { getMaxVariable = nr } ) "MAX_VARS" )
   "maximum number of variables (freedom of constraint problem) [10000]"

  , Option ['r'] ["use-relax-rule"]
   (NoArg (\opts -> return $ opts { getUseRelaxRule = True } ))
   "include the relax inference rule (this may increase computation time)."

  , Option ['s'] ["use-smt-solver"]
   (NoArg (\opts -> return $ opts { getSMTSolver = True } ))
   "use the SMT solver instead of the LP solver as backend."

  , Option ['t'] ["max-tree-height"]
   (ReqArg (\str opts -> do
              let readRes = reads str :: [(Int, String)]
              if null readRes
              then return (opts { getMaxTreeHeight = -1 })
              else do
                let [(nr, _)] = readRes
                return $ opts { getMaxTreeHeight = nr } ) "MAX_HEIGHT" )
   "maximum height of inference tree [100]"

  , Option ['v'] ["verbose"]
   (NoArg (\opts -> return $ opts { getVerbose = True } ))
   "verbose output"

  ]


-- |This function parses the Arguments. It gets them and then parses it, and
-- returns a ArgumentOptions object or throws an ioError Exception.
--
-- There is the possiblitiy of optionally giving the filePath. If it is not
-- given, then the default filepath will be taken and a warning will be
-- displayed.
parseArgOpts :: (Monad m) => IO (m ArgumentOptions)
parseArgOpts = do
  argv <- getArgs                                      -- get arguments
  progName <- getProgName                              -- get Program name
  tmpDir <- getTemporaryDirectory
  let                                                  -- create help text
      header = "Usage: " ++ progName ++ " [OPTION...] filePath"
      helpMessage = usageInfo header options

      (o, files, err) = getOpt Permute options argv
  -- case errors occured, throw exception, else call functions for each option
  -- selected
  if not $ null err
  then throw $ FatalException $ concat err ++ "\n" ++ helpMessage
  else do
    opt <- foldlM (flip id) (defaultOptions { getTempDir = tmpDir }) o
    if getHelpText opt
    then throw $ ShowTextOnly helpMessage
    else
        if getMaxVariable opt < 1 || getMaxTreeHeight opt < 1
        then
          throw (FatalException $
                 "Max-variable and max-tree-height argument must be integers > 0.\n\n" ++
                helpMessage)
        else
            case files of
              [] -> throw $ ShowTextOnly ("FATAL ERROR: No input file was given.\n\n" ++ helpMessage)
              (f:_) -> return $ return $ opt { getFilePath = f }


--
-- ArgumentOptions.hs ends here
