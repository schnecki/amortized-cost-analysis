(* * * * * * * * * * *
 * Resource Aware ML *
 * * * * * * * * * * *
 * File:
 *   queue.raml
 *
 * Authors:
 *   Martin Hofmann, Jan Hoffmann (2009)
 *
 * Resource Bound:
 *   O(n^2)
 *
 * Description:
 *   A breadth-first traversal of a tree using a FIFO queue.
 *
 *   A nice example that demonstrates the strengths of amortized
 *   analysis is the analysis of algorithms that uses a FIFO queue or
 *   just a queue.  A queue is usually implemented as a pair of lists
 *   that consists of an in-queue and an out-queue: If an element is
 *   enqueued then it is added to the top of the in-queue.  If an
 *   element is dequeued then it is removed from the top of the
 *   out-queue.  If the out-queue is empty then the in-queue is copied
 *   over to the out-queue.
 *
 *   A standard example of the use of a queue is the breadth-first
 *   traversal of a tree: One puts the tree in the queue and then
 *   proceeds by dequeing an element, processing its root, and
 *   enqueueing its children.
 *
 *   Here we use a compact representaition of special trees that has the 
 *   type (int,int,L(int),L(int)).  The size of the tree is quadratic in the
 *   lengths of the lists.  For example (1,1,[2,3,4,5],[2,3,4,5])
 *   defines the tree
 *
 *              (1,1)
 *             /     \
 *          (2,1)     (2,2)
 *           /       /    \
 *        (3,1)   (3,2)   (3,3)
 *         /       /       /   \
 *      (4,1)   (4,2)   (4,3)  (4,4)
 *       /       /       /      /   \
 *    (5,1)   (5,2)   (5,3)  (5,4)  (5,5)
 *
 *   See the function children for details.
 *
 *   This compact representation of trees leads to a quadratic resource 
 *   behavior.
 *)


empty : unit -> (L(int,int,L(int),L(int)),L(int,int,L(int),L(int)))
(* The empty queue.*)

empty x = ([],[]);


enqueue : ((int,int,L(int),L(int)), (L(int,int,L(int),L(int)),L(int,int,L(int),L(int))) ) 
          -> (L(int,int,L(int),L(int)),L(int,int,L(int),L(int)))
(*enqueue a tree in a queue of trees.*)

enqueue(x,queue) = let (inq,outq) = queue in 
                   (x::inq,outq);

enqueues : (L(int,int,L(int),L(int)),(L(int,int,L(int),L(int)),L(int,int,L(int),L(int)))) 
           -> (L(int,int,L(int),L(int)),L(int,int,L(int),L(int)))
(*enqueue a LIST of trees in a queue of trees.*)

enqueues (l,queue) = match l with 
                       | [] -> queue 
                       | (x::xs) -> enqueues (xs,(enqueue (x,queue)));



copyover : (L(int,int,L(int),L(int)),L(int,int,L(int),L(int))) 
        -> (L(int,int,L(int),L(int)),L(int,int,L(int),L(int)))
(* A helper function for dequeue.  It copies the in-queue over if
 * the out-queue is empty. *)

copyover queue = let (inq,outq) = queue in
                 match inq with
                   | [] -> ([],outq)
                   | (x::xs) -> copyover(xs,x::outq);

(* Note: the left component of the output of copyover is constantly nil;
   therefore all anotations with non-zero degree for that component are
   unconstrained. This will be reported as a Warning by our analysis tool. *)


dequeue : (L(int,int,L(int),L(int)),L(int,int,L(int),L(int))) 
          -> ((L(int,int,L(int),L(int)),L(int,int,L(int),L(int))),L(int,int,L(int),L(int)))
(* Dequeues a tree from a queue of trees.
 * It returns the new queue and the dequeued tree.
 * Since the queue might be empty.  We return not just a tree
 * but a list of trees.  It is a singleton list if the queue was not
 * empty and nil otherwise. *)

dequeue queue  = let (inq,outq) = queue in
	         match outq with
                    | [] -> match inq with
                              | [] -> (([],[]),[])
                              | (x::xs) -> dequeue(copyover (x::xs,[]) )
                    | (y::ys) -> ((inq,ys),[y]);


children : (int,int,L(int),L(int)) -> ((int,int),L(int,int,L(int),L(int)))
(* children takes a tree and returns a pair consisting of
 *   - the root of the tree
 *   - a list of the children of the root
 * There can 1,2 or 0 children. *)

children (a,b,l1,l2) = ((a,b),
                        match l1 with
       			   | [] -> match l2 with
                                     | [] -> []
                                     | (y::ys) -> [(y,b,[],ys)]
                           | (x::xs) -> match l2 with
                                          | [] -> []
				          | (y::ys) -> [(x,b,[],xs),(x,y,xs,ys)]
			    );


breadth : (L(int,int,L(int),L(int)),L(int,int,L(int),L(int))) -> L(int,int)
(* A breadth-first traversal on the tree like it is done in textbooks. *)

breadth queue = let (queue',elem) = dequeue queue in
                match elem with
                  | [] -> []
                  | (z::_) -> let (x,ys) = children z in 
                              x :: breadth(enqueues(ys,queue'));

startBreadth : L(int) -> L(int,int)
(* Start a breadth-first traversal on the tree that is defined by the list. *)

startBreadth xs = match xs with
                    | nil -> nil
                    | (x::xs) -> breadth (enqueue ((x,x,xs,xs),empty ())); 


(*

(* One can of course also implement a depth-first traversal.
 * But this is not so interesting.  Note however that the functions
 * below admit a cubic resource behavior. *)

appendD : (L(int,int),L(int,int)) -> L(int,int)

appendD (xs,ys) = matchD xs with
                   | nil -> ys
                   | (x::xs) -> x::appendD(xs,ys);


depth : (int,int,L(int),L(int)) -> L(int,int)

depth (a,b,l1,l2) = let (x,zs) = children (a,b,l1,l2) in
                    x :: match zs with
                           | [] -> []
                           | (z1::zs) -> match zs with
                                           | [] -> depth z1
                                           | (z2::_) -> appendD(depth z1,depth z2);

startDepth : L(int) -> L(int,int)

startDepth xs = match xs with 
               | nil -> nil
               | (x::xs) -> depth (x,x,xs,xs);

*)



main = startBreadth [+1,+2,+3,+4,+5,+6]
