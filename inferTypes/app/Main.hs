module Main where

-- import Termlib.Types
-- import Termlib.Utils
-- import Termlib.Problem.Parser
-- import Termlib.Trs
-- import Termlib.Problem
-- import Termlib.Signature
-- import Termlib.FunctionSymbol

import Infer

import           Data.Rewriting.Problem
import           Data.Rewriting.Rule
import           Data.Rewriting.Signature


import System.Directory
import Control.Monad.State
import System.Process
import qualified Data.Text as T

main :: IO ()
main = do

  trssUnfilterd <- readProcess "find" [".", "-name", "*.trs"] []
  let trss = filter (not . T.isInfixOf (T.pack "typed"))
             (T.lines $ T.pack trssUnfilterd)

  mapM_ createTypedTrs trss

  -- trsStr <- readFile trsPath

createTypedTrs :: T.Text -> IO ()
createTypedTrs trsPath = do
  prob <- parseFileIO (T.unpack trsPath)
  let prob' = inferTypesAndSignature prob

  let out = show (prettyWST' prob')
  let fileOut = T.unpack $ T.replace (T.pack ".trs") (T.pack ".typed.trs") trsPath

  writeFile fileOut out

