\documentclass[
%draft,
handout,
]{beamer}

\usetheme[shadow]{ComputationalLogic}

% \usepackage{pgfpages}
% \pgfpagesuselayout{4 on 1}[a4paper,landscape,border shrink=5mm]

\usepackage{amsmath,amssymb,alltt}
\usepackage[utf8x]{inputenc}
\usepackage{xcolor}
\usepackage[english]{babel}
\usepackage{stmaryrd}
%\usepackage{wasysym}
%\usepackage{manfnt}
%\usepackage{pifont}
%\usepackage{isabelle}
\RequirePackage{etex}
\usepackage{tikz}
\usetikzlibrary{backgrounds}
\usetikzlibrary{fit}
\usetikzlibrary{matrix}
\usetikzlibrary{shapes.symbols}
\usetikzlibrary{shapes.callouts}
\usetikzlibrary{arrows}
\usetikzlibrary{trees}
\usetikzlibrary{shadows}
\usetikzlibrary{automata}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{fadings}
\usepackage{listings}
\usepackage{turnstile}
\usepackage[numbers %numern als referenzen
]{natbib}   % better bibliography
\usepackage{proof}

\setbeamertemplate{sidebar right}{}
\setbeamertemplate{sections/subsections in toc}[ball unnumbered]
\setbeamersize{text margin left=5mm,text margin right=5mm}
\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{enumerate items}[square]
\setbeamertemplate{qed symbol}{\vrule width 1.5ex height 1.5ex depth 0pt}
\setbeamertemplate{blocks}[rounded][shadow=true]
\setbeamertemplate{boxes}

\setbeamertemplate{navigation symbols}{%
  \hbox{%
    \hbox{\insertslidenavigationsymbol}
    \hbox{\insertframenavigationsymbol}
    \hbox{\insertsectionnavigationsymbol}
    \hbox{\insertbackfindforwardnavigationsymbol}%
  }%
}
\setbeamertemplate{seal opacity}{.3}

\newcommand{\GREEN}[1]{{\color{green!50!black}#1}}
\newcommand{\BLACK}[1]{{\color{black}#1}}
\newcommand{\RED}[1]{{\alert{#1}}}
\newcommand{\DARKRED}[1]{{\color{red!50!black}#1}}
\newcommand{\DARKBLUE}[1]{{\color{blue!50!black}#1}}
\newcommand{\BLUE}[1]{{\color{blue}#1}}
\newcommand{\MAGENTA}[1]{{\color{magenta}#1}}
\newcommand{\GRAY}[1]{{\color{gray}#1}}
\newcommand{\DGRAY}[1]{{\color{gray!40!black}#1}}
\newcommand{\YELLOW}[1]{{\color{yellow}#1}}
\newcommand{\WHITE}[1]{{\color{white}#1}}
\newcommand{\DARKGREEN}[1]{{\color{green!50!black}#1}}

\newenvironment{mybox}[1]{\begin{beamerboxesrounded}[shadow=true]{\large #1}}{\end{beamerboxesrounded}}
\newenvironment{tctsays}{\begin{beamerboxesrounded}[shadow=true]{\large \tct\ says}}{\end{beamerboxesrounded}}
\newenvironment{question}[1]{\begin{beamerboxesrounded}[shadow=true]{\MAGENTA{Question~{#1}}}}{\end{beamerboxesrounded}}
\newenvironment{answer}{\begin{beamerboxesrounded}[shadow=true]{\MAGENTA{Answer}}}{\end{beamerboxesrounded}}
\newcommand{\questionbox}{\large\fbox{\phantom{$\checkmark$}}}
\newcommand{\questionticbox}{\large\fbox{$\checkmark$}}

\lstset{language=ML,
basicstyle=\footnotesize\color{green!50!black}
%basicstyle=\smallsize\ttfamily,
}

\usepackage{slides}

\title[July 15, 2014]{Amortised Resource Analysis and Typed Polynomial Interpretations}
% author(s) of paper
\author[GM]{Martin Hofmann \and \textbf{Georg Moser}}
\institute[ICS]{%
\inst{1}Institut für Informatik\\
Ludwig-Maximilians-Universität München
\and
\inst{2}Institut für Informatik\\
Universität Innsbruck
}
\date{RTA/TLCA'14: July 15, 2014}

\begin{document}
\titleframe

\frame{
\transdissolve[duration=0.2]

\frametitle{Overview}

\tableofcontents

}

\section{Quiz}

\begin{frame}[fragile]
\begin{example}[``A common implementation of purely functional queues'']
%  (* Okasaki: Fig 5.2 
%  * 
%  * type Queue(nat) = (L(nat),L(nat))
%  * keeps the invariant that first element is non-empty list
%  *
%  *)
\vspace{-4mm}
\begin{lstlisting}
empty x = (nil,nil);

checkF (f,r) = match f with 
                | nil -> (rev(r),nil)
                | (x::xs) -> (f,r);

snoc (queue,x) = match queue with 
                | (f,r) -> checkF(f,x::r);

enq n = match n with 
                | 0 -> empty()
                | S n' -> snoc(enq(n'),n');
 
main = enq 3;                                      
main = ([0],[3,2,1])
\end{lstlisting}
\end{example}

\onslide<2->
\medskip

\lstset{basicstyle=\normalsize\color{green!50!black}}
\begin{mybox}{Comment}
queues are implemented as pairs of list $f$ and $r$; \lstinline{checkF} guarantees the
invariant that $f$ is only empty, if the queue is empty
\end{mybox}
\end{frame}

\lstset{basicstyle=\normalsize\color{green!50!black}}

\begin{frame}

\begin{question}{1}
What is the complexity of \lstinline!enq n!?\\[-4ex]
\begin{center}
\begin{tabular}{l@{\qquad}l@{\qquad}l}
linear? \alt<1>{\questionbox}{\questionticbox} & quadratic? \questionbox & exponential? \questionbox 
\end{tabular}
\end{center}
\end{question}

\medskip
\onslide<3->

\begin{definitions}
\begin{itemize}
\item the \RED{potential function} $\Phi$ is a mapping from values 
to $\N$
\item suppose $d_i$ is the output of the $i^{th}$ operation and input to
the $i+1^{th}$ operation; $t_i$ the actual cost of the $i^{th}$ operation
\item the \RED{amortised cost} of operation $i$ is defined as follows:\\[-2ex]
  \begin{equation*}
    \RED{a_i} = t_i + \Phi(d_i) - \Phi(d_{i-1})
  \end{equation*}
\end{itemize}
\end{definitions}

\medskip
\onslide<4->

\begin{mybox}{Bounding the Total Cost}
\vspace{-.5cm}
$$
    \sum_{i=1}^j t_i = \sum_{i=1}^j \left( a_i + \Phi(d_{i-1}) - \Phi(d_i) \right)
    = \sum_{i=1}^j  a_i + \Phi(d_0) - \Phi(d_j) \leqslant \sum_{i=1}^j  a_i
$$
\end{mybox}

\end{frame}

\begin{frame}[fragile]

\begin{example}[cont'd]
\color{green!50!black}
\begin{itemize}
\item we assign to every queue the length of the 2nd list as potential
\item \lstinline!snoc! has constant amortised costs
\item \lstinline!enq! is linear
\end{itemize}
\end{example}

\onslide<2->
\medskip
\begin{mybox}{Automation}
{\footnotesize
\begin{verbatim}
$raml solve analyse eval-steps 1 queue.raml

The number of evaluation steps consumed by enq is at most:
          36.0*n + 7.0
where n is the value of the input
\end{verbatim}}
\end{mybox}

\onslide<3->
\medskip
\bibfont\footnotesize
\begin{thebibliography}{1}

\bibitem{HAH12b}
J.~Hoffmann, K.~Aehlig, and M.~Hofmann.
\newblock Multivariate amortized resource analysis.
\newblock {\em ACM Trans. Program. Lang. Syst.}, 34(3):14, 2012.

\bibitem{HAH:2012}
J.~Hoffmann, K.~Aehlig, and M.~Hofmann.
\newblock Resource aware {ML}.
\newblock In {\em Proc.\ 24th CAV}, volume 7358 of {\em LNCS}, pages 781--786,
  2012.

\end{thebibliography}
\end{frame}

\begin{frame}
\frametitle{From Functional Programming to Rewriting}

\begin{question}{}
How to generalise to rewrite systems (= arbitrary data structures)?
\end{question} 

\bigskip
\onslide<2->

\begin{example}[Okasaki's Example as TRS $\RS$]
\vspace{-4mm}
\color{green!50!black}
\begin{alignat*}{4}
&& \alert<3>{\checkF(\queue(\nil,r))} &\alert<3>{\to \queue(\rev(r),\nil)}
&
\hspace{2ex}
&& \alert<3>{\enq(\zero)} &\alert<3>{\to \queue(\nil,\nil)}
\\[2mm]
&& \alert<3>{\checkF(\queue(x \cons xs,r))} &\alert<3>{\to \queue(x \cons xs,r)}
&
\hspace{2ex}
&& \revp(\nil,ys) & \to ys
\\[2mm]
&& \tail(\queue(x \cons f,r)) &\to \checkF(\queue(f,r))
&
\hspace{2ex}
&& \rev(xs) &\to  \revp(xs,\nil)
\\[2mm]
&& \alert<3>{\snoc(\queue(f,r),x)} &\alert<3>{\to \checkF(\queue(f,x \cons r))}
&
\hspace{2ex}
&& \head(\queue(x \cons f,r)) &\to x
\\[2mm]
&& \revp(x \cons xs,ys) &\to \revp(xs,x \cons ys)
&
\hspace{2ex}
&& \head(\queue(\nil,r)) &\to \errorHead
\\[2mm]
&& \alert<3>{\enq(\mS(n))} &\alert<3>{\to \snoc(\enq(n),n)}
&
\hspace{2ex}
&& \tail(\queue(\nil,r)) &\to \errorTail
\end{alignat*}
\end{example}
\end{frame}

\section{Typed Term Rewrite Systems}
\begin{frame}
\begin{definitions}
\begin{itemize}
\item<1-> let $S$ be a finite set of (data) \alert<1>{types}
  % let $\CS$ denote \alert<1>{constructor symbols};  
  % $\DS$ denote \alert<1>{defined function symbols}
\item<2-> a \alert<2>{type declarations} is of the form 
$\typdcl{A_1 \times \cdots \times A_n}{C}$,\\
where $A_i$ and $C$ are types; in the following terms are typed
%  ; the set of terms is denoted as $\TA(\CS \cup \DS,\VS)$
\item<3-> an \alert<3>{$S$-typed term rewrite system} (\alert<3>{TRS} for short)
$\RS$ is a finite set of typed rewrite rules
\end{itemize}
\end{definitions}

\medskip
\onslide<4->
\begin{mybox}{Comment}
we restrict to \RED{constructor-based}, \RED{orthogonal} TRSs in conjunction
with \RED{innermost} rewriting
\end{mybox}

\medskip
\onslide<5->
  
\begin{definition}
\begin{itemize}
\item<5-> $\alert<4>{\dheight}(t,\to) = \max \{ n \mid \exists u \; t \to^n u \}$
\item<6-> a term $f(t_1,\dots,t_n)$ is \alert<6-7>{\alt<6>{basic}{nice}} if $f$ is
    defined and $\forall$ $i$: $t_i \in \TA(\CS,\VS)$
\item<8-> $\alert<5>{\rc_\RS}(n) = \max \{ \dheight(t,\rsrew) \mid \text{$t$ is nice and $\size{t} \leqslant n$}\}$
\end{itemize}
\end{definition}
\end{frame}

\begin{frame}[t]
\uncover<3->{
\begin{definition}[Operational Big-Step Semantics]
\begin{center}
\begin{tabular}{c} 
   $\infer{\eval{\sigma}{0}{x}{v}}{%
     x\sigma = v%
     }$
   \hfill
   $\infer{\eval{\sigma}{0}{c(x_1,\dots,x_n)}{c(v_1,\dots,v_n)}}{%
       c \in \CS
       &
       x_1\sigma = v_1 
       & \cdots
       & x_n\sigma = v_n%
     }$
   \\[2mm]
   $\infer{\eval{\sigma}{m+1}{f(x_1,\ldots,x_n)}{v}}{%
     f(l_1,\ldots,l_n) \to r \in \RS
     & \exists \tau \ \forall i\colon x_i\sigma = l_i\tau
     & \eval{\sigma \dunion \tau}{m}{r}{v}
   }$
   \\[2mm]
   $\infer{\eval{\sigma}{m}{f(t_1,\ldots,t_n)}{v}}{%
     \begin{minipage}[b]{30ex}
       all $x_i$ are fresh \hfil
       \\[.5ex]
       $\eval{\sigma \dunion \rho}{m_0}{f(x_1,\ldots,x_n)}{v}$ \hfill
     \end{minipage}
     & \eval{\sigma}{m_1}{t_1}{v_1}
     & \cdots
     & \eval{\sigma}{m_n}{t_n}{v_n}
   }$
\end{tabular}
\end{center}
\vspace{-5mm}
{\small
\begin{itemize}
\item $\rho = \{x_1 \mapsto v_1,\ldots,x_n \mapsto v_n\}$
\item $\sigma$, $\tau$, and $\rho$ are normalised
\end{itemize}
}
  % Here $\rho \defsym \{x_1 \mapsto v_1,\ldots,x_n \mapsto v_n\}$.
  % Recall that $\sigma$, $\tau$, and $\rho$ are normalised. 
\end{definition}
}

\medskip
\begin{mybox}{Proposition}
% Let $f$ be a defined function symbol of arity $n$ and $\sigma$
% a normalised substitution.
$\eval{\sigma}{\alert<2->{m}}{f(x_1,\dots,x_n)}{t}$ iff\/ 
$\dheight(f(x_1\sigma,\dots,x_n\sigma),\rsrew) = \alert<2->{m}$
\end{mybox}
\end{frame}

\section{Amortised Resource Analysis}
\frame{
\transdissolve[duration=0.2]

\begin{center}
\BLUE{\huge{Amortised Resource Analysis}}  
\end{center}
}

\begin{frame}
%\frametitle{Annotated Types}  

\begin{definitions}
\begin{itemize}
\item<1->
an \alert<1->{annotated type} $\atyp{A}{\vec{p}}$ is a pair of
\begin{enumerate}
\item a type $A \in S$
\item a vector $\vec{p}=(p_1,\dots,p_k)$ over $\N$
\end{enumerate}
%The vector $\vec{p}$ is called \emph{resource annotation}. 

\item<2->
let $f \in \CS \cup \DS$ with type declaration 
$\typdcl{A_1 \times \cdots \times A_n}{C}$

\item<2->
an \alert<2-4>{annotated type declaration} for $f$ is a type declaration over
annotated types, decorated with $p \in \N$:\\[-2ex]
%
\begin{equation*}
  \atypdcl{A_1^{\alert<3,4>{\vec{u_1}}} \times \cdots \times A_n^{\alert<3,4>{\vec{u_n}}}}{\atyp{C}{\alert<3,4>{\vec{v}}}}{\alert<4>{p}}
\end{equation*}

\item<5->
an \alert<5>{annotated signature} (or simply \alert<5>{signature})
is a mapping $\FS \colon \CS \cup \DS \to (\pow(\TDannot)\setminus \varnothing)$ 
\end{itemize}
\end{definitions}

\medskip
\onslide<6->

\begin{example}
\color{green!50!black}
\vspace{-1mm}
\begin{center}
$\FS(\zero) = \alt<6>{\typdcl{}{\Nat}}{\{\atypdcl{}{\aNat{\RED{\vec p}}}{\RED{0}} \mid \RED{\vec{p}}\in \Vecs\}}$ \hspace{5ex}
$\FS(\mS) = \alt<6>{\typdcl{\Nat}{\Nat}}{\{\atypdcl{\aNat{\RED{\shift(\vec p)}}}{\aNat{\RED{\vec p}}}{\RED{\Diamond\vec p}}\mid \RED{\vec{p}}\in \Vecs\}}$
\end{center}
\vspace{-2mm}
\onslide<8->
{\small
where if $\vec{p}=(p_1,\dots,p_k)$ then 
$\alert<8->{\shift}(\vec{p})=(p_1+p_2,p_2+p_3,\dots,  p_{k-1} + p_k, p_k)$\\
and $\alert<8->{\Diamond}\vec{p} = p_1$}
\end{example}
\end{frame}

\begin{frame}
\begin{definition}[Potential]
\begin{itemize}
\item let $v = c(v_1,\dots,v_n) \in \TA(\CS)$ and let $C$ be an annotated type
\item the \RED{potential} $\RED{\Phi}(\typed{v}{C})$ of $v$ under $C$
    is defined as follows:\\[-2ex]
    \begin{equation*}
      \RED{\Phi}(\typed{v}{C}) = p + \RED{\Phi}(\typed{v_1}{A_1}) + \cdots 
      + \RED{\Phi}(\typed{v_n}{A_n})
    \end{equation*}
    %
    where $\atypdcl{A_1 \times \cdots \times A_n}{C}{p} \in \FS(c)$
\end{itemize}
\end{definition}

\medskip
\onslide<2->

\begin{example}
\color{green!50!black}
\vspace{-6mm}
\begin{alignat*}{4}
&\text{\MAGENTA{$n$ of type $\Nat$}}  %\hspace{5ex}
& \Phi(\typed{n}{\aNat{0}}) &= 0 & \hspace{-2ex}
&& \Phi(\typed{n}{\aNat{k}}) & = k \cdot n
\\
&\text{\MAGENTA{$l$ a list}}
& 
\Phi(\typed{l}{\aList{(p,q)}}) &= p \cdot \len{l} + q \cdot \binom{\len{l}}{2}
\\[-1cm]
\end{alignat*}
{\small
where $\typed{\nil}{\atypdcl{}{\aList{\vec p}}{0}}$ and
$\typed{\cons}{\atypdcl{\aNat{0} \times \aList{\shift(\vec p)}}{\aList{\vec p}}{\Diamond\vec p}}$}
\end{example}

\medskip
\onslide<3->

\begin{definition}
$\alert<3>{\share{A^{\vec{p}}}{A^{\vec{p_1}},A^{\vec{p_2}}}}$ holds if $\vec{p_1} + \vec{p_2} = \vec p$  
\end{definition}

\end{frame}

\begin{frame}
\begin{definition}[Type System for Rewrite Systems (selection)]
\begin{center}
  \begin{tabular}{c}
    \uncover<1->{$\infer{\tjudge{\typed{x_1}{A_1},\dots,\typed{x_n}{A_n}}{p}{\typed{f(x_1,\dots,x_n)}{C}}}{%
      f \in \CS \cup \DS
      &
      \atypdcl{A_1 \times \cdots \times A_n}{C}{p} \in \FS(f)
    }$}
    \hfill 
    \uncover<3->{$\infer{\tjudge{\typed{x}{A}}{0}{\typed{x}{A}}}{}$}
    \\[3mm]
    \uncover<2->{$\infer{\tjudge{\Gamma_1,\dots,\Gamma_n}{p}{\typed{f(t_1,\dots,t_n)}{C}}}{%
      \tjudge{\typed{x_1}{A_1},\dots,\typed{x_n}{A_n}}{p_0}{\typed{f(x_1,\dots,x_n)}{C}}
      &
      \tjudge{\Gamma_1}{p_1}{\typed{t_1}{A_1}} \ \cdots \
        \tjudge{\Gamma_n}{p_n}{\typed{t_n}{A_n}}
      }$}
    \\[3mm] 
    \uncover<3->{$\infer{\tjudge{\Gamma, \typed{x}{A}}{p}{\typed{t}{C}}}{%
      \tjudge{\Gamma}{p}{\typed{t}{C}}
      }$}
    \hfill
    \uncover<4->{$\infer{\tjudge{\Gamma, \typed{z}{A}}{p}{\typed{t[z,z]}{C}}}{%
      \tjudge{\Gamma, \typed{x}{A_1}, \typed{y}{A_2}}{p}{\typed{t[x,y]}{C}}%
      & 
      \share{A}{A_1,A_2}
      }$}    
  \end{tabular}    
\end{center}
\end{definition}

\medskip
\onslide<5->

\begin{theorem}
  \begin{enumerate}
  \item let $\RS$ and $\sigma$ be \alert<6->{well-typed}
  \item suppose $\tjudge{\Gamma}{p}{\typed{t}{A}}$ and $\eval{\sigma}{m}{t}{v}$
  \end{enumerate}
then $\Phi(\typed{\sigma}{\Gamma}) - \Phi(\typed{v}{A}) + p  \geqslant m$  
\end{theorem}
\end{frame}

\begin{frame}[t]
\uncover<3->{%
\begin{mybox}{Type Rule}
\vspace{-3mm}
\begin{equation*}
\only<3| handout:0>{%
\infer{\tjudge{\Gamma, \typed{z}{A}}{p}{\typed{t[z,z]}{C}}}{%
      \tjudge{\Gamma, \typed{x}{A_1}, \typed{y}{A_2}}{p}{\typed{t[x,y]}{C}}%
      & 
      \share{A}{A_1,A_2}
      }%
}%
\only<4| handout:0>{%
\infer{\tjudge{\Gamma_1,\dots,\Gamma_n}{p}{\typed{f(t_1,\dots,t_n)}{C}}}{%
      \tjudge{\typed{x_1}{A_1},\dots,\typed{x_n}{A_n}}{p_0}{\typed{f(x_1,\dots,x_n)}{C}}
      &
      \tjudge{\Gamma_1}{p_1}{\typed{t_1}{A_1}} \ \cdots \
        \tjudge{\Gamma_n}{p_n}{\typed{t_n}{A_n}}
      }%
}%
\only<5>{%
\infer{\tjudge{\typed{x_1}{A_1},\dots,\typed{x_n}{A_n}}{p}{\typed{f(x_1,\dots,x_n)}{C}}}{%
      f \in \CS \cup \DS
      &
      \atypdcl{A_1 \times \cdots \times A_n}{C}{p} \in \FS(f)
    }%
}%
\end{equation*}
\end{mybox}}

\medskip

\begin{example}
\color{green!50!black}
consider the following type declarations\\[-1ex]
%
\begin{center}
\begin{tabular}{ll}
$\typed{\queue}{\atypdcl{\aList{0} \times \aList{1}}{\aQueue{(0,1)}}{0}}$
&
$\typed{\zero}{\atypdcl{}{\aNat{6}}{0}}$
\quad
$\typed{\mS}{\atypdcl{\aNat{6}}{\aNat{6}}{6}}$
\\
$\snoc\colon \atypdcl{\aQueue{(0,1)} \times \aNat{0}}{\aQueue{(0,1)}}{5}$
&
$\typed{\enq}{\atypdcl{\aNat{6}}{\aQueue{(0,1)}}{1}}$
\end{tabular}
\end{center}
%
\uncover<2->{%
together with the following derivation:\\[-2ex]
\begin{equation*}
  \infer{\tjudge{\typed{n}{\aNat{6}}}{6}{\typed{\snoc(\enq(n),n)}{\aQueue{(0,1)}}}}{%
    \infer{\tjudge{\typed{n_1}{\aNat{6}},\typed{n_2}{\aNat{0}}}{6}{\typed{\snoc(\enq(n_1),n_2)}{\aQueue{(0,1)}}}}{%
      \tjudge{\typed{q}{\aQueue{(0,1)}},\typed{m}{\aNat{0}}}{5}{\typed{\snoc(q,m)}{\aQueue{(0,1)}}}
      &
      \tjudge{\typed{n_1}{\aNat{6}}}{1}{\typed{\enq(n_1)}{\aQueue{(0,1)}}}
      &
      \dots      
    }
  }
\end{equation*}}
\vspace{-8mm}
\end{example}  

%\onslide<5->
\end{frame}

\begin{frame}
\begin{definition}
\vspace{-1mm}
\begin{itemize}
\item let ${f(l_1,\dots,l_n) \to r} \in {\RS}$ and 
  let $\Var(f(\vec{l})) = \{y_1,\dots,y_\ell\}$
\item suppose $\forall$ types $B_{j}$ and $\forall$ costs \alert<2>{$k_i$} such that
$\tjudge{\typed{y_{1}}{B_{1}},\dots,\typed{y_{\ell}}{B_{\ell}}}{\alert<2>{k_i}}{\typed{l_i}{A_i}}$
we have\\[-2ex]
\begin{equation*}
\tjudge{\typed{y_{1}}{B_{1}},\dots,\typed{y_{\ell}}{B_{\ell}}}{\alert<2>{p - 1 + \sum_{i=1}^n k_i}}{\typed{r}{C}}
\end{equation*}
\end{itemize}
%
then $\typed{f}{\atypdcl{A_1 \times \cdots \times A_n}{C}{\alert<2>{p}}}$ is \RED{well-typed}; $\RS$ is \RED{well-typed} if any defined $f$ is well-typed
\end{definition}

\medskip
\onslide<3->
\begin{example}
\vspace{-1mm}
\color{green!50!black}
the motivating TRS is well-typed with the given type declarations
\end{example}

\medskip
\onslide<4->
\begin{corollary}
let $\RS$ be well-typed; suppose for each constructor\\[-2ex]
\begin{equation*}
  \typed{c}{\atypdcl{A_1^{\vec{u_1}} \times \cdots \times A_n^{\vec{u_n}}}{C^{\vec{w}}}{p}}
\vspace{-2mm}
\end{equation*}
%
we have $\len{\vec{w}} \leqslant k$ (and some mild further conditions)
then $\rc_\RS(n) \in \bO(n^k)$   
\end{corollary}
\end{frame}

\section{Typed Polynomial Interpretations}
\frame{
\transdissolve[duration=0.2]
\begin{center}
\BLUE{\huge{Typed Polynomial Interpretations}}  
\end{center}
}

\begin{frame}
\begin{definitions}
\begin{itemize}
\item<1-> to each annotated type $C$, we assign its
  \alert<1>{interpretation} $\interdomain{C} \subseteq \N$

\item<2-> consider $\typed{f}{\atypdcl{A_1 \times \cdots \times A_n}{C}{p}}$

\item<3-> the \RED{interpretation of $f$} is defined as:\\[-2ex]
  \begin{equation*}
    \gamma(f,C)(x_1,\dots,x_n) = x_1 + \cdots + x_n + p
  \end{equation*}

\item<4-> interpretations of function symbols naturally 
extend to interpretations on ground terms, denoted
as \RED{$\groundinter{\typed{t}{C}}$}

\end{itemize}
\end{definitions}
% Note that by assumption the declaration in $\FS(f)$ is unique and thus
% $\gamma(f,C)$ is unique.

\medskip
\onslide<5->

\begin{example}[continued]
\color{green!50!black}
from the earlier type declarations of $\queue$, $\mS$, $\snoc$, and 
$\enq$ we obtain\\[-4ex]
\begin{alignat*}{4}
&& \gamma(\queue,\aQueue{(0,1)})(f,r) & = f + r & \hspace{2ex}
&& \gamma(\mS,\aNat{6})(n) & = n + 6
\\
&& \gamma(\snoc,\aQueue{(0,1)})(q,n) &= q + n + 5 & \hspace{2ex}
&& \gamma(\enq,\aQueue{(0,1)})(n) &= n + 1
\end{alignat*}
\end{example}
\end{frame}

\begin{frame}
\begin{example}
\color{green!50!black}
consider rule $\enq(\mS(n)) \to \snoc(\enq(n),n)$\\[-4ex]
\begin{align*}
 \groundinter{\typed{\enq(\mS(n))}{\aQueue{(0,1)}}} & 
  \uncover<2->{= \groundinter{\typed{\mS(n)}{\aNat{6}}} + 1} 
  \uncover<3->{= \groundinter{\typed{n}{\aNat{6}}} + 7}  
  \\
  & > \uncover<4->{\groundinter{\typed{n}{\aNat{6}}} + 6\\}
  & \uncover<5->{= \groundinter{\typed{\enq(n)}{\aQueue{(0,1)}}} + \groundinter{\typed{n}{\aNat{0}}} + 5}
  \\
  & \uncover<6->{=} \groundinter{\typed{\snoc(\enq(n),n)}{\aQueue{(0,1)}}}
\end{align*}
\end{example}

\medskip
\onslide<7->

\begin{mybox}{Comment}
for a suitable extension of $\Phi$ from values
to all ground terms, we have \alert<7>{$\groundinter{\typed{t}{A}} = \Phi(\typed{t}{A})$}
\end{mybox}

\medskip
\onslide<8->

\begin{theorem}
let $\RS$ be well-typed and let $\gamma$ 
be induced by the type system, then $l > r$ for any rule ${l \to r} \in \RS$;
furthermore if for all ground basic terms $t$, $\size{t} \leqslant n$ and types $A$: 
$\groundinter{\typed{t}{A}} \in \bigO(n^k)$, then $\rc_{\RS}(n) \in \bigO(n^k)$
\end{theorem}
\end{frame}

\section{Conclusion}
\begin{frame}
\frametitle{Conclusion}

\begin{mybox}{Summary}
  \begin{itemize}
  \item generalisation of amortised analysis to typed rewrite systems
  \item novel technique of typed linear interpretation
  \item $\exists$ prototype implementations for the case that induces
    linear complexity 
  \end{itemize}
\end{mybox}

\bigskip
\onslide<2->

\begin{mybox}{Future Work}
  \begin{itemize}
  \item termination on basic terms is a \RED{persistent} property
  \item same for \RED{runtime complexity}
  \item types can be approximated to apply the results to
    runtime complexity analysis of standard TRSs
  \end{itemize}
\end{mybox}
\end{frame}

\frame{
\transdissolve[duration=0.2]

\begin{center}
\BLUE{\huge{Thank You for Your Attention!}}  
\end{center}
}

% \nocite{HAH:2012,HAH12b}
% \bibliographystyle{abbrv} 
% \bibliography{references}
\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
