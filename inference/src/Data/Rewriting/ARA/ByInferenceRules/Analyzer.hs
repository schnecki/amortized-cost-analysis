-- Analyzer.hs ---
--
-- Filename: Analyzer.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Thu Sep  4 23:59:10 2014 (+0200)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Tue Apr 11 14:33:59 2017 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 9
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--

--
--

-- Code:

-- | TODO: comment this module
module Data.Rewriting.ARA.ByInferenceRules.Analyzer
    ( -- reexporting modules
      module Data.Rewriting.ARA.ByInferenceRules.Analyzer.Ops
    )
    where

import           Data.Rewriting.ARA.ByInferenceRules.Analyzer.Ops
--
-- Analyzer.hs ends here
